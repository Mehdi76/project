import Categories from "@/components/Landing/Categories";
import Header from "@/components/common/Header";
import Intro from "@/components/Landing/Intro";
import Services from "@/components/Landing/Services";
import Suggest from "@/components/Landing/Suggest";
import TeacherSection from "@/components/Landing/TeacherSection";
import Footer from "@/components/common/Footer";
import React from "react";
import LandingFinal from "@/components/Landing";

const Landing = () => {
  return (
    <>
     <LandingFinal/>
    </>
  );
};

export default Landing;
