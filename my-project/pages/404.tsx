import NoFound from "@/components/Errors/NoFound";
import React from "react";

const NotFound = () => {
  return (
    <>
      <NoFound />
    </>
  );
};

export default NotFound;
