import React from "react";
import LoginPage from "@/components/Login/Login";
import http from "http";
import { GetServerSideProps } from "next";
import { getCookie } from "@/core/services/storage/cookies.storage";

const Login = ({ token }: any) => {
  console.log(token)
  if (token) {
    return <div>biadab</div>;
  }
  return (
    <div>
      <LoginPage />
    </div>
  );
};
export const getServerSideProps: GetServerSideProps = async (context) => {
  const cookieHeader = context.req.headers.cookie;
  const token = cookieHeader ? getCookie("token", cookieHeader) : null;
  if(token){
    return {
      redirect: {
        permanent: false,
        destination: "/404"
      }
    };
  }
  return {
    props: {
      token: token,
    },
  };
};

export default Login;
