import SideBar from '@/components/StudentPanel/SideBar'
import Layout from '@/components/StudentPanel/Layout'
import React from 'react'
import DashboardCom from '@/components/StudentPanel/DashboardCom'
import axios from 'axios';
import { GetServerSideProps, GetStaticProps } from 'next';
import { getCookie } from '@/core/services/storage/cookies.storage';


const Dashboard = ({user}: any) => {
  return (
  <div style={{width:"100%"}}>
    <SideBar />
    <Layout >
      <DashboardCom user={user} />
    </Layout>
  </div>
  )
}
type DataResult ={
  result:Result[]
} 
interface Result {
  _id: string
  role : string
  fullName: string
  email: string
  phoneNumber: number
  birthDate: number
  nationalId: number
  profile: string
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const cookieHeader = context.req.headers.cookie;
  const token = cookieHeader ? getCookie("user", cookieHeader) : null;
  
  return {
    props: {
      user: token,
    },
};
}
export default Dashboard