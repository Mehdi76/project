import Layout from '@/components/StudentPanel/Layout'
import MyCourses from '@/components/StudentPanel/MyCourses'
import SideBar from '@/components/StudentPanel/SideBar'
import { getCookie } from '@/core/services/storage/cookies.storage'
import { GetServerSideProps } from 'next'
import React from 'react'

const index = ({course}:any) => {
  return (
    <div style={{width:"100%"}}>
    <SideBar />
    <Layout>
        <MyCourses course={course}/>
    </Layout>
    </div>
  )
}
type DataResult ={
  result:Result[]
} 
interface Result {
  _id: string;
  title: string;
  cost: number;
  endDate: string;
  startDate: string;
  capacity: number;
  image: string;
  description: string;
  teacher: {
    fullName: string;
    email: string;
    _id: string;
    profile: string;
  };
  lesson: {
    _id: string;
    lessonName: string;
    topics: string[];
    image: string;
  };
}
export const getServerSideProps: GetServerSideProps = async (context) => {
  const cookieHeader = context.req.headers.cookie;
  const token = cookieHeader ? getCookie("course", cookieHeader) : null;
  
  
  return {
    props: {
      course: token,
    },
};
}
export default index