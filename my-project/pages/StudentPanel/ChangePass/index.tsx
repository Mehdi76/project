import ChangePass from '@/components/StudentPanel/ChangePass'
import Layout from '@/components/StudentPanel/Layout'
import SideBar from '@/components/StudentPanel/SideBar'
import React from 'react'


const index = () => {
  return (
  <div style={{width:"100%"}}>  
   <SideBar />
    <Layout>
      <ChangePass/>
    </Layout>
    </div>
  )
}

export default index