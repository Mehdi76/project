import EditInfo from '@/components/StudentPanel/EditInfo'
import Layout from '@/components/StudentPanel/Layout'
import SidBar from '@/components/StudentPanel/SideBar'
import React from 'react'

const index = () => {
  return (
  <div style={{width:"100%"}}>
    <SidBar />
    <Layout>
      <EditInfo/>
    </Layout>
    </div>
  )
}

export default index