import NewsDetailFinal from "@/components/NewsDetail";
import axios from "axios";
import { GetServerSideProps } from "next";
import React from "react";

const NewsDetail = ({ result , allResult}: any) => {
  console.log(allResult);

  return (
    <>
      <NewsDetailFinal data={result} allData={allResult} />
    </>
  );
};
type DataResult = {
  result: Result[];

};
interface Result {}
export const getServerSideProps: GetServerSideProps = async (context) => {
  const id = context.params?.newId;
  const BASE_URL = process.env.NEXT_PUBLIC_APP_URL;
  const response = await axios.get(`${BASE_URL}/news/${id}`);
  const responseAllNews = await axios.get(`${BASE_URL}/news`);
  


  const data: DataResult = await response.data.result;
  const allData: DataResult = await responseAllNews.data.result;
  return {
    props: {
       result: data,
      allResult : allData
    },
  };
};

export default NewsDetail;
