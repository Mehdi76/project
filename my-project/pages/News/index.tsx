import NewsFinal from "@/components/News";
import type { InferGetServerSidePropsType, GetServerSideProps } from "next";
import React, { useEffect } from "react";
import axios from "axios";

type DataResult ={
  result:Result[]
}
interface Result {
  title: string;
  text: string;
  category: string;
  image: string;
}
export const getServerSideProps: GetServerSideProps = async () => {
  // Fetch the data from an API or any other sou
  const BASE_URL = process.env.NEXT_PUBLIC_APP_URL;
  const response = await axios.get("/news",{baseURL:BASE_URL});
  const data: DataResult = await response.data;

  return {
    props: {
      result: data.result,
    },
  };
};

const News = ({ result }: { result: Result[] }) => {
  return (
    <>
      <NewsFinal data={result} />
    </>
  );
};

export default News;
