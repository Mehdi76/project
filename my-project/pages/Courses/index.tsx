import CoursesFinal from '@/components/Courses';
import type { InferGetServerSidePropsType, GetServerSideProps } from "next";
import React, { useEffect } from "react";
import axios from "axios";

type DataResult ={
  result:Result[]
}
interface Result {
    title: string,
    cost:string ,
    endDate:string ,
    startDate: string,
    capacity: number,
    teacher:string ,
    lesson: string
}
export const getServerSideProps: GetServerSideProps = async () => {
  // Fetch the data from an API or any other sou
  const BASE_URL = process.env.NEXT_PUBLIC_APP_URL;
  const response = await axios.get("/course/getall",{baseURL:BASE_URL});
  const data: DataResult = await response.data;

  return {
    props: {
      result: data.result,
    },
  };
};

const Courses = ({ result }: { result: Result[] }) => {
  return (
    <>
     <CoursesFinal data={result} />
   </>
  )
}

export default Courses