import CoursesDetailFinal from "@/components/CoursesDetail"
import axios from "axios";
import { GetServerSideProps } from "next";
import React from "react";


const Course = ({ result, allData }: any) => {

  return (
    <>
      <CoursesDetailFinal data={result} allData={allData} />
    </>
  )
}
type DataResult = {
  result: Result[];
};
interface Result {
  _id: string;
  title: string
  cost: number
  endDate: string
  startDate: string
  capacity: number
  image: string
  description: string
  teacher: {
    fullName: string
    email: string
    _id: string
    profile: string
  },
  lesson: {
    _id: string
    lessonName: string
    topics: string[],
    image: string
  },
}
export const getServerSideProps: GetServerSideProps = async (context) => {
  const id = context.params?.newId;
  const BASE_URL = process.env.NEXT_PUBLIC_APP_URL;
  /* const response = await axios.get(`${BASE_URL}/lesson/${id}`); */
  const responseAllCourses = await axios.get(`${BASE_URL}/course/getall`);

  const allData: Result[]= await responseAllCourses.data.result;
 const bookmark =allData.findIndex(i => i._id === id )
 const data: Result | boolean = bookmark !== -1 ? allData[bookmark] : false

  return {
    props: {
      result: data,
      allData:allData
    },
  };
};
export default Course