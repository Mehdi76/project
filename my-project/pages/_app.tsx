import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { ThemeProvider } from "next-themes";
import Preloader from "@/components/common/PreLoader";
import { HistoryContextProvider } from "@/core/context/HistoryContext";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <HistoryContextProvider>
      <ThemeProvider attribute="class">
        <Component {...pageProps} />
        <Preloader />
      </ThemeProvider>
    </HistoryContextProvider>
  );
}
