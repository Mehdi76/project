import instace from "../../../interceptor";
import LoginUser from "../Login/login-api";

type User = {
  fullName: string;
  email: string;
  password: string;
  phoneNumber: string;
  birthDate: string;
  nationalId: string;
  profile: string;
};


export const RegisterUser = async (user: User) => {
  try {
    const result = await instace.post("auth/register", user);
    const Res = result as any;
    const res = Res.result;
    await LoginUser({email:user.email,password:user.password})
    console.log(res);
    return res;
  } catch (error) {
    console.log(error);
    return false;
  }
};
