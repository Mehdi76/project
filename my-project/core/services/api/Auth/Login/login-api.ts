import instace from "../../../interceptor";
import { setCookie } from "../../../storage/cookies.storage";

type User = {
  email: string;
  password: string;
};

const LoginUser = async (user: User) => {
  try {
    const result = await instace.post("auth/login", user);
    const Ress = result as any;
    const res = Ress.result;
    setCookie("token", res?.jwtToken, 7);
    setCookie("user", res?.studentModel, 7);
    return res;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export default LoginUser;
