import instace from "../../interceptor";
import { setCookie } from "../../storage/cookies.storage";

type User = {
  email: string;
};

export const forgetPassUser = async (user: User) => {
  try {
    const result = await instace.post("auth/forgetpassword", user);
    const res = result.data.result;
    setCookie("token", res?.jwtToken, 7);
    setCookie("user", res?.studentModel, 7);
    console.log(res);
    return res;
  } catch (error) {
    console.log(error);
    return null;
  }
};
