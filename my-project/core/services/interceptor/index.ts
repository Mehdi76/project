import axios from "axios";
import { getCookie } from "../storage/cookies.storage";

const BASE_URL = process.env.NEXT_PUBLIC_APP_URL;

const instance = axios.create({
  baseURL: BASE_URL,
});

instance.interceptors.response.use(
  (res) => {
    // console.log("res", res);
    return res.data;
  },
  (err) => {
    console.log(err);
    const response = err.response;

    if (response?.status >= 400 && response?.status < 500) {
      console.log(response.data.message);
    }
    return Promise.reject(err);
  }
);

instance.interceptors.request.use((config) => {
  const token = getCookie("token");
  config.headers["Example"] = "TEST";
  if (token) config.headers["x-auth-token"] = token;
  return config;
});

export default instance;
