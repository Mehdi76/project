
// export default HistoryContextProvider;
import { createContext, useState, useEffect } from "react";
export const HistoryContext = createContext();
export const HistoryContextProvider = (props) => {
  const [history, setHistory] = useState([]);
  const handleHistory = (array) => {
    setHistory((oldItem) => [...oldItem, array]);
   
  };
  const handleDelete = (id) => {
    const newData = history.filter((item) => item.courseId != id);
    setHistory(newData);
  };
  useEffect(() => {
    console.log(history);
  }, [history]);

  return (
    <HistoryContext.Provider value={{ history, handleHistory, handleDelete }}>
      {props.children}
    </HistoryContext.Provider>
  );
};
/* import { createContext, useState, useEffect, ReactNode } from "react";

interface History {
  _id: string;
  title: string;
  cost: number;
  endDate: string;
  startDate: string;
  capacity: number;
  image: string;
  description: string;
  teacher: {
    fullName: string;
    email: string;
    _id: string;
    profile: string;
  };
  lesson: {
    _id: string;
    lessonName: string;
    topics: string[];
    image: string;
  };
}

interface HistoryContextType {
  history: History[];
  handleHistory: (array: History) => void;
  handleDelete: (id: string) => void;
}

export const HistoryContext = createContext<HistoryContextType | null>(null);

type HistoryContextProviderProps = {
  children: ReactNode;
};

export const HistoryContextProvider = ({ children }: HistoryContextProviderProps) => {
  const [history, setHistory] = useState<History[]>([]);

  const handleHistory = (array: History) => {
    setHistory((oldItem) => [...oldItem, array]);
    // console.log(array)
  };

  const handleDelete = (id: string) => {
    const newData = history.filter((item) => item.courseId !== id);
    setHistory(newData);
  };

  useEffect(() => {
    console.log(history);
  }, [history]);

  return (
    <HistoryContext.Provider value={{ history, handleHistory, handleDelete }}>
      {children}
    </HistoryContext.Provider>
  );
}; */