import React from "react";
import FormSuggest from "../Landing/FormSuggest";
import Header from "../common/Header";
import Image from "next/image";
import Footer from "../common/Footer";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

const ContactUs = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <>
      <div>
        <Header />
      </div>
      <section
        data-aos="fade-left"
        data-aos-duration="2000"
        className="flex justify-between max-w-7xl m-auto"
      >
        <div className="mr-24 mb-28 sm:ml-28 sm:-mt-4 lg:mt-32">
          <h1 className=" text-blue-900 font-bold text-5xl mb-2 sm:text-center lg:text-right ">
            ارتباط با آموزشگاه بحر
          </h1>
          <hr className="lg:border-yellow-300 lg:border-2 hidden lg:block w-20  mb-8 sm:mr-8 lg:-mr-1" />

          <div className="sm:mt-14">
            <span className="text-xl text-gray-600 ">
              برای یادگیری کامل و اصولی برنامه نویسی به همراه اساتید مجرب با ما
              همراه شوید.{" "}
            </span>
          </div>
        </div>

        <div className=" sm:hidden lg:block hidden ">
          <Image
            src="/assets/images/ContactUs/intros.png"
            alt="intro"
            width={500}
            height={530}
          />
        </div>
      </section>

      <div  data-aos="zoom-in" data-aos-duration="3000" className="relative flex items-top justify-center  bg-white dark:bg-gray-900 sm:items-center sm:pt-0">
        <div className="max-w-6xl mx-auto sm:px-6 lg:px-8">
          <div className="mt-8 overflow-hidden h-[630px]">
            <div className="grid gap-28 grid-cols-1 md:grid-cols-2">
              <div className="sm:-mt-32 md:relative top-32 mx-auto">
                <FormSuggest />
              </div>

              <div className="w-[400px] h-[465px] shadow-xl rounded-xl mt-28  p-6 mr-2 bg-sky-100 dark:bg-gray-800 sm:rounded-lg">
                <h1 className="mt-4 text-center text-4xl sm:text-3xl text-gray-800 dark:text-white font-bold tracking-tight">
                  ارتباط با ما
                </h1>
                <p className="mt-4 text-center text-normal text-lg sm:text-xl font-medium text-gray-600 dark:text-gray-400 ">
                  برای شروع لطفا فرم را پر کنید{" "}
                </p>

                <div className="flex items-center mt-12 text-gray-600 dark:text-gray-400 mb-5">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="1.5"
                    viewBox="0 0 24 24"
                    className="w-8 h-8 text-gray-500"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="1.5"
                      d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
                    />
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="1.5"
                      d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"
                    />
                  </svg>
                  <div className="ml-4 text-md tracking-wide font-semibold w-[288px]">
                    آدرس :ساری٫ بلوار خزر٫ بعد از دنیای آرزو
                  </div>
                </div>

                <div className="flex items-center mt-4 text-gray-600 dark:text-gray-400 mb-5">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="1.5"
                    viewBox="0 0 24 24"
                    className="w-8 h-8 text-gray-500"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="1.5"
                      d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z"
                    />
                  </svg>
                  <div className="ml-4 text-md tracking-wide font-semibold w-[288px]">
                    شماره تماس : -۰۹۳۳۳۵۸۴۲۵۱
                  </div>
                </div>

                <div className="flex items-center mt-2 text-gray-600 dark:text-gray-400 mb-5">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="1.5"
                    viewBox="0 0 24 24"
                    className="w-8 h-8 text-gray-500"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="1.5"
                      d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"
                    />
                  </svg>
                  <div className="ml-4 text-md tracking-wide font-semibold w-[288px]">
                    آدرس ایمیل :BahrAgency@Academy.Com
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="-mb-10">
        <Footer />
      </div>
    </>
  );
};

export default ContactUs;
