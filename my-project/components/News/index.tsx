import React from "react";
import Header from '@/components/common/Header'
import Footer from "@/components/common/Footer";
import NewsCard from "@/components/News/NewsCard";
import Title from '@/components/common/Title';
import Intro from "@/components/News/Intro";
interface Result {
  title: string;
  text: string;
  category: string;
  image: string;
}
type Props={
data:Result[]
}
const NewsFinal = ({data}:Props) => {
  return (
    <>
      <Header />
      <Intro />
      <section className="mt-40 md:mt-10  ">
        <Title title="لیست تمام اخبار و مقالات" style="text-center text-blue-950 text-4xl border-r-4 border-yellow-300 inline-block px-4 mb-6 w-62" />
        <NewsCard data={data} />
      </section>
      <Footer />
    </>
  );
};

export default NewsFinal;