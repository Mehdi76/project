import React, { useEffect } from 'react'
import Title from '../common/Title'
import Image from 'next/image'
import AOS from 'aos';
import 'aos/dist/aos.css';

const Intro = () => {
  useEffect(() => {
    AOS.init()
}, [])
  return (
    <>
      <nav data-aos="fade-right" data-aos-duration="2000" className="md:grid grid-cols-2 h-96 mx-auto mt-20 xl:mt-14 text-center -mb-40  md:mb-20 " style={{ width: "80%" }} >
        <div className="mt-15 m-auto   ">
          <Title title="اخبار و مقالات دنیای تکنولوژی " style="text-blue-900 font-bold text-4xl mb-2 pb-4 mt-6" />
          <hr className="border-yellow-300 border-2 w-20 mb-8 mr-14" />
          <h2 className=" text-xl text-gray-600 pr-2	leading-loose">با تازه‌‌ترین دستاوردهای دنیای تکنولوژی در بخش اخبار و مقالات آشنا شوید و دانش خود را بروز نمایید 
          </h2>
        </div>
        <div >
          <figure >
            <Image
              src="/assets/images/News/News.PNG"
              alt="courses"
              height={0}
              width={500}
              className="md:flex sm:mr-10 m-auto hidden   "
            />
          </figure>
        </div>
      </nav>   
    </>
  )
}

export default Intro