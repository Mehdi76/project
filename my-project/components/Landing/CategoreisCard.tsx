import Image from "next/image";
import React from "react";

interface Props {
  src: string;
  title: string;
  under: string;
  card: string;
}

const CategoreisCard = (props: Props) => {
  return (
    <div>
      <div className={props.card}>
        <Image src={props.src} alt="no" height={53} width={53} className="m-auto " />
        <span className={props.under}>{props.title}</span>
      </div>
    </div>
  );
};

export default CategoreisCard;
