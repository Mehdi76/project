import Image from "next/image";
import React from "react";
import Title from "../common/Title";
import CategoreisCard from "./CategoreisCard";

const cate = [
  {
    src: "/../public/assets/images/Landing/architect.png",
    title: "معماری",
    under: "border-b-4 border-red-500 flex justify-center shadow-xl  ",
    card: "opacity-0 shadow-xl  dark:bg-white",
  },
  {
    src: "/../public/assets/images/Landing/atom.png",
    title: "شیمی",
    under:
      "border-b-4 border-cyan-500 flex justify-center shadow-xl rounded-md",
    card: " shadow-xl rounded-md  dark:bg-white",
  },
  {
    src: "/../public/assets/images/Landing/calc.png",
    title: "ریاضی",
    under: "border-b-4 border-sky-500 flex justify-center shadow-xl rounded-md",
    card: " shadow-xl rounded-md  dark:bg-white",
  },
  {
    src: "/../public/assets/images/Landing/factory.png",
    title: "صنایع",
    under:
      "border-b-4 border-orange-500 flex justify-center shadow-xl rounded-md",
    card: " shadow-xl rounded-md dark:bg-white",
  },
  {
    src: "/../public/assets/images/Landing/growth.png",
    title: "سهام",
    under:
      "border-b-4 border-fuchsia-500 flex justify-center shadow-xl rounded-md",
    card: " shadow-xl rounded-md  dark:bg-white",
  },
  {
    src: "/../public/assets/images/Landing/mouse.png",
    title: "کامپیوتر",
    under:
      "border-b-4 border-orange-600 flex justify-center shadow-xl rounded-md",
    card: " shadow-xl rounded-md  dark:bg-white",
  },
  {
    src: "/../public/assets/images/Landing/socket.png",
    title: "برق",
    under:
      "border-b-4 border-purple-700 flex justify-center shadow-xl rounded-md",
    card: " shadow-xl rounded-md  dark:bg-white",
  },
  {
    src: "/../public/assets/images/Landing/test-tube.png",
    title: "حسابداری",
    under:
      "border-b-4 border-green-400 flex justify-center shadow-xl rounded-md",
    card: " shadow-xl rounded-md  dark:bg-white",
  },
  {
    src: "/../public/assets/images/Landing/architect.png",
    title: "معماری",
    under: "border-b-4 border-red-500 flex justify-center shadow-xl rounded-md",
    card: " shadow-xl rounded-md w-24  dark:bg-white",
  },
];

const Categories = () => {
  return (
    <section  className="max-w-5xl m-auto mt-48">
      <div className="sm:mb-24 lg:mb-14">
      <Title title="دسته بندی ها" style="text-blue-900 dark:text-white text-4xl border-r-4 border-yellow-300 w-52 m-auto"/>
      </div>

      <div data-aos="zoom-out" data-aos-duration="3000" className="grid xl:grid-cols-2 mb-44   lg:grid-cols-2 md:grid-cols-1  ">
        <div className=" justify-center m-auto sm:hidden  lg:block hidden ">
          <Image
            src="/assets/images/Landing/categories.png"
            alt="categories"
            className=" sm:mb-12 ml-20"
            height={484}
            width={390}
          />
        </div>

        <div className="grid mt-16  md:m-auto md:w-10/12  sm:m-auto  ">
          <div className="flex justify-center  ">
            <div className="grid md:grid-cols-3 gap-14 sm:grid-cols-2 ">
              {cate.map((item, index) => (
                <CategoreisCard
                  key={index}
                  src={item.src}
                  title={item.title}
                  under={item.under}
                  card={item.card}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Categories;
