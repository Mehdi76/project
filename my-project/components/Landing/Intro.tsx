import Image from "next/image";
import Button from "../../components/common/Button";
import Link from "next/link";
import AOS from "aos";
import "aos/dist/aos.css";
import style from "../../styles/Landing.module.css"
import { useEffect } from "react";

const Intro = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <section
      data-aos="fade-left"
      data-aos-duration="1500"
      className="flex justify-between max-w-7xl m-auto "
    >
      <div className="mt mr-24 mb-5 sm:ml-28 sm:-mt-4 lg:mt-24">
        <h1 className=" text-blue-900 dark:text-white font-bold text-5xl mb-2 sm:text-center lg:text-right ">
          آکادمی کد نویسی بحر
        </h1>
        <hr className="lg:border-yellow-300 lg:border-2 hidden lg:block w-20  mb-8 sm:mr-8 lg:-mr-1" />
        <div className="sm:mt-14">
          <span className="text-xl text-gray-600 dark:text-gray-400">
            برای یادگیری کامل و اصولی برنامه نویسی به همراه اساتید مجرب با ما
            همراه شوید.{" "}
          </span>
        </div>
        <div className="sm:flex m-auto mt-24 sm:justify-center ml-10 mb-24 grid md:grid-cols-2 grid-cols-1 ">
          <div className="ml-3">
            <Link href="../Courses">
              <Button
                title="شروع یادگیری"
                class="active:bg-green-700 hover:bg-green-500 w-52 h-12 bg-emerald-500 text-teal-50 rounded-md shadow-2xl"
              />
            </Link>
          </div>
          <div className="-ml-14  sm:mt-px">
            <Link href="../News">
              <Button
                title="مشاهده مقالات"
                class="active:bg-blue-700 active:text-white hover:bg-blue-500 w-52 h-12 bg-sky-400 text-teal-50 rounded-md shadow-2xl"
              />
            </Link>
          </div>
        </div>
      </div>
        <div className={style.scrollDown}></div>
      

      <div className="ml- sm:hidden lg:block hidden ">
        <Image
          src="/assets/images/Landing/intro.png"
          alt="intro"
          width={550}
          height={530}
        />
      </div>
    </section>
  );
};

export default Intro;
