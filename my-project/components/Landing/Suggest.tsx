import React from "react";
import Image from "next/image";
import Title from "../common/Title";
import FormSuggest from "./FormSuggest";
import style from "../../styles/Landing.module.css";

const Suggest = () => {
  return (
    <section >
      <Title title="انتقادات و پیشنهادات" style="text-blue-900 dark:text-white text-4xl border-r-4 border-yellow-300 w-80 m-auto"/>
      <div data-aos="zoom-in" data-aos-duration="3000" className="flex justify-center ">
        <div className="grid md:grid-cols-2 w-fit gap-20 sm:grid-cols-1">
          <div className="">
            <FormSuggest />
          </div>
          <div className="mt-36 md:block hidden ">
            <Image
              src="/assets/images/Landing/girl.png"
              alt="girl"
              height={419}
              width={380}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Suggest;
