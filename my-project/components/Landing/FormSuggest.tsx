import React from "react";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as yup from "yup";
import Button from "../common/Button";

const FormSuggest = () => {
  const validation = yup.object().shape({
    name: yup
      .string()
      .required(" لطفا نام خود را وارد کنید ")
      .matches(/^[آ-ی]*$/, " فرمت نام اشتباه می باشد"),
    email: yup
      .string()
      .email("فرمت ایمیل اشتباه می باشد")
      .required(" لطفا ایمیل خود را وارد کنید "),

    message: yup
      .string()
      .required(" لطفا متن پیام  را وارد کنید ")
      .min(3, "حداقل 3 کاراکتر وارد کنید")
      .max(10, "حداکثر 100 کاراکتر وارد کنید "),
  });

  const handleSubmit = (value: any) => {
    console.log(value);
  };
  return (
    <section className=" ">
      <div className="mt-28 h-auto w-72 shadow-xl rounded-xl p-4 ">
        <Formik
          initialValues={{ email: "", name: "", message: "" }}
          onSubmit={handleSubmit}
          validationSchema={validation}
        >
          <Form className="text-center">
            <div className="h-16 ">
              <label className="ml-44 dark:text-white" htmlFor="">
                نام کاربر
              </label>
              <Field
                name="name"
                type="text"
                className="p-4 h-9 w-60 mt- border border-gray-300 rounded-lg  dark:bg-white "
              />
              <ErrorMessage
                name="name"
                render={(msg) => (
                  <p
                    style={{
                      color: "red",
                      fontSize: "14px",
                      marginLeft: "105px",
                    }}
                  >
                    {msg}
                  </p>
                )}
              />
            </div>

            <br />

            <div className="h-16 ">
              <label className="ml-44 dark:text-white" htmlFor="">
                ایمیل
              </label>
              <Field
                name="email"
                type="email"
                className="p-4 h-9 w-60 border border-gray-300 rounded-lg dark:bg-white "
              />
              <ErrorMessage
                name="email"
                render={(msg) => (
                  <p
                    style={{
                      color: "red",
                      fontSize: "14px",
                      marginLeft: "105px",
                    }}
                  >
                    {msg}
                  </p>
                )}
              />
            </div>
            <br />
            <div className="h-36 ">
              <label className="ml-44 dark:text-white" htmlFor="">
                متن پیام
              </label>
              <Field
                name="message"
                type="text"
                className="p-4 h-28 w-60 border border-gray-300 rounded-lg dark:bg-white"
              />
              <ErrorMessage
                name="message"
                render={(msg) => (
                  <p
                    style={{
                      color: "red",
                      fontSize: "14px",
                      marginLeft: "105px",
                    }}
                  >
                    {msg}
                  </p>
                )}
              />
            </div>
          </Form>
        </Formik>
        <div className="flex justify-center mt-8 ">
          <Button
            title="ثبت پیام"
            class="active:bg-green-700 hover:bg-green-500 w-32 h-8 bg-emerald-400 rounded-md text-white "
          />
        </div>
      </div>
    </section>
  );
};

export default FormSuggest;
