import Image from "next/image";
import React from "react";

interface Props {
  name: string;
  fontColor: string;
  holderStyle: string;
  srcc: string;
}
// tip =()=> map must be out of parent for not have 4 component in a div , it must be a one per element

const SerivceCards = (props: Props) => {
  return (
    <div className="">
      <div className={props.holderStyle}>
        <Image
          src={props.srcc}
          width={95}
          height={95}
          alt="no"
          className="inline-block"
        />
        <div className="block text-center mt-8 ">
          <span className={props.fontColor}>{props.name}</span>
        </div>
      </div>
    </div>
  );
};

export default SerivceCards;
