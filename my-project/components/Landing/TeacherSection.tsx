import React from "react";
import TeacherCard from "./TeacherCard";
import Title from "../common/Title";
import style from "../../styles/Landing.module.css"

const data = [
  {
    name: "دکتر محمد بحر العلوم",
    description: "مدرس فرانت و بک",
    image: "/assets/images/Landing/teacher.png",
    animation:"fade-right",
    delay:"500"
  },
  {
    name: "استاد مهدی اصغری",
    description: "مدرس ری اکت",
    image: "/assets/images/Landing/teacherr.png",
    animation:"fade-right",
    delay:"1000"
  },
  {
    name: "استاد محسن اسفندیاری",
    description: "مدرس نکست",
    image: "/assets/images/Landing/teacher.png",
    animation:"fade-right",
    delay:"1500"
  },
  {
    name: "استاد امیرحسین بهبودی",
    description: "مدرس جاوا اسکریپت",
    image: "/assets/images/Landing/teacherr.png",
    animation:"fade-right",
    delay:"2000"
  },
];

const TeacherSection = () => {
  return (
    <section className="-mt-8">
      <Title title="اساتید برتر" style="text-blue-900 dark:text-white text-4xl border-r-4 border-yellow-300 w-40 m-auto"/>
      <div data-aos="fade-right"  data-aos-duration="1000" className="flex justify-center mt-20 mb-44">
      <div className="gap-28 sm:grid grid-cols-2  lg:grid-cols-4 ">
          {data.map((item, index) => (
            <TeacherCard
              name={item.name}
              description={item.description}
              image={item.image}
              key={index}
              anim={item.animation}
              delay={item.delay}
            />
          ))}
        </div>
        </div>
    </section>
  );
};

export default TeacherSection;
