import React from "react";
import Header from "../common/Header";
import Intro from "./Intro";
import Services from "./Services";
import Categories from "./Categories";
import TeacherSection from "./TeacherSection";
import Footer from "../common/Footer";
import Suggest from "./Suggest";
import Progress  from "./Progress";

const LandingFinal = () => {
  return (
    <>
      <div className="max-w-7xl m-auto overflow-hidden min-w-[480px] dark:bg-slate-900    ">
        <Header />
        <Progress/>
        <Intro />
        <Services />
        <Categories />
        <TeacherSection />
        <Suggest />
        <Footer />
      </div>
    </>
  );
};

export default LandingFinal;
