import Image from "next/image";
import React from "react";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

interface Props {
  name: string;
  description: string;
  image: string;
  anim: string;
  delay: number;
}

const TeacherCard = (props: Props) => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <section
      data-aos={props.anim}
      data-aos-delay={props.delay}
      className="bg-slate-50 shadow-lg rounded-md"
    >
      <div className="text-center">
        <Image
          src={props.image}
          className="shadow-lg"
          alt="teacher"
          height={174}
          width={175}
        />
        <h3 className="mt-4 text-center">{props.name}</h3>
        <p className="text-center">{props.description}</p>
      </div>
      <div className="flex justify-start mr-10 mt-8 mb-2">
        <Image
          src="/assets/images/Landing/whatsapps.png"
          alt="whatsapps"
          height={30}
          width={30}
        />
        <Image
          src="/assets/images/Landing/twitters.png"
          alt="twitters"
          height={30}
          width={30}
        />
        <Image
          src="/assets/images/Landing/youtubes.png"
          alt="youtubes"
          height={30}
          width={30}
        />
      </div>
    </section>
  );
};

export default TeacherCard;
