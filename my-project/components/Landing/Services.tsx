import Image from "next/image";
import React from "react";
import Title from "../common/Title";
import SerivceCards from "./SerivceCards";

const service = [
  {
    srcc: "/../public/assets/images/Landing/chat.png",
    name: "مشاوره",
    fontColor: "flex text-violet-500 justify-center ",
    holderStyle: "lg:mt-4 m-4 shadow-xl p-8 rounded-xl sm:mb-8 dark:bg-white ",
  },
  {
    srcc: "/../public/assets/images/Landing/test.png",
    name: "امتحان",
    fontColor: "flex text-red-500 justify-center",
    holderStyle: "lg:-mb-4 m-4 shadow-xl p-8 rounded-xl sm:mt-8 dark:bg-white",
  },
  {
    srcc: "/../public/assets/images/Landing/microphone.png",
    name: "فرصت های شغلی",
    fontColor: "flex text-sky-500 justify-center",
    holderStyle: "lg:mt-4 m-4 shadow-xl p-8 rounded-xl sm:mb-8 dark:bg-white",
  },
  {
    srcc: "/../public/assets/images/Landing/trophy.png",
    name: "مدرک معتبر",
    fontColor: "flex text-orange-500 justify-center ",
    holderStyle: "lg:-mb-4 m-4 shadow-xl p-8 rounded-xl sm:mt-8 dark:bg-white",
  },
];

const Services = () => {
  return (
    <section >
      <div className="-mb-16">
        <div className=""></div>
        <Title
          title="خدمات"
          style="text-blue-900 dark:text-white text-4xl border-r-4 border-yellow-300 w-36 m-auto"
        />
      </div>

      <div data-aos="flip-right" data-aos-duration="3000" data-aos-delay="300" className="mt-24 mb-32">
        <section className="flex justify-center">
          <div className=" sm:grid grid-cols-2 place-items-center mt-10 lg:grid-cols-4 gap-12 ">
            {service.map((item, index) => (
              <SerivceCards
                key={index}
                name={item.name}
                fontColor={item.fontColor}
                holderStyle={item.holderStyle}
                srcc={item.srcc}
              />
            ))}
          </div>
        </section>
      </div>
    </section>
  );
};

export default Services;
