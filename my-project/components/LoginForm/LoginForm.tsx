import React, { useState } from "react";
import Image from "next/image";
import {
  ErrorMessage,
  Field,
  Form,
  Formik,
  FormikProvider,
  useFormik,
} from "formik";
import * as yup from "yup";
import InputForm from "../common/InputForm";
import Button from "../common/Button";
import Link from "next/link";
import LoginUser from "@/core/services/api/Auth/Login/login-api";
import { redirect } from "next/dist/server/api-utils";
import { useRouter } from "next/router";
import eyeicon from "../../public/assets/images/Common/eyeicon.png";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const LoginForm = () => {
  const notify = () => toast("با موفقیت وارد شدید");

  const [passwordShown, setPasswordShown] = useState(true);

  const route = useRouter();

  const validation = yup.object().shape({
    email: yup
      .string()
      .email("فرمت ایمیل اشتباه می باشد")
      .required(" لطفا ایمیل خود را وارد کنید "),

    password: yup.string().required("  رمز عبور باید شامل 8 کاراکتر و 1 حرف بزرگ و 1 حرف کوچک و 1 عدد و 1 کاراکتر خاص باشد")
    /* .matches(
      /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/,
      "فرمت پسورد اشتباه است"
    ), */
  });

  const handleSubmit = async (value: { email: string; password: string }) => {
    const val = { email: value.email, password: value.password };
    const login = await LoginUser(val);
    if (login) {
      toast.success("با موفقیت وارد شدید");
      route.push("/");
    } else {
      toast.error("ورود  با موفقیت انجام نشد");
    }
  };

  const formikLogin = useFormik({
    initialValues: { email: "", password: "" },
    onSubmit: handleSubmit,
    validationSchema: validation,
  });
  return (
    <>
      <div className="w-[380px] h-[470px] border-2 border-green-500 rounded-xl mt-16">
        <div>
          <div className="-mt-4 mr-6 text-center bg-white w-24 h-8 ">
            <h3 className="text-green-600 text-xl">ورود کاربر</h3>
            <div>
              <FormikProvider value={formikLogin}>
                <Form className="text-center mt-4">
                  <div className="h-16 ">
                    <InputForm title="ایمیل کاربر" name="email" />
                  </div>
                  <br />
                  <div className="h-24 ">
                    <InputForm
                      title=" رمزعبور"
                      name="password"
                     /*  types={passwordShown ? "password" : "text"} */
                    />
                   
                   {/*  <Image
                     className="cursor-pointer absolute left- "
                      src={eyeicon}
                      height={30}
                      width={25}
                      alt="none"
                      onClick={() => {
                      setPasswordShown(!passwordShown);
                      }}
                    /> */}
                  </div>
                </Form>
              </FormikProvider>
            </div>
            <div className="text-sm text-sky-500 w-36 mt-2">
              <Link href={"/ForgetPass"}>
                <p>رمز عبورم را فراموش کرده ام</p>
              </Link>
            </div>

            <div className="flex w-36 text-md mt-4 ">
              
            
            </div>
            <div className=" fixed mt-10">
              <div className="ml-2 ">
                <Button
                  onClick={(e) => {
                    e.preventDefault();
                    formikLogin.handleSubmit();
                  }}
                  // onClick={notify}
                  title="ورود"
                  type="submit"
                  class=" active:bg-green-700 hover:bg-green-500 w-72 p-2  bg-green-400 text-teal-50 rounded-md shadow-2xl"
                />
                <ToastContainer />
              </div>
              <div className="mt-5">
                <Link href={"/Register"}>
                  <Button
                    title="ثبت نام  "
                    class="active:bg-blue-400 active:text-white ml-3   w-32 p-2 bg-emerald-50-400 text-sky-500 border border-sky-500 rounded-md shadow-2xl"
                  />
                </Link>
                <Link href={"http://localhost:2020"}>
                  <Button
                    title="ورود کارمندان"
                    class="active:bg-blue-400 active:text-white  ml-3 w-32 p-2 bg-emerald-50-400 text-sky-500 border border-sky-500 rounded-md shadow-2xl"
                  />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginForm;
