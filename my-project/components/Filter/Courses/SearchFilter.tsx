import { useEffect, useState } from "react";

interface Result {
  title:string
  cost:number
  endDate: string
  startDate: string
  capacity: number
  image: string
  description: string
  teacher: {
      fullName: string
      email: string
      _id: string
      profile: string
  },
  lesson: {
    _id: string
    lessonName: string
    
image:string
topics:string[]
  },
}
interface Props  {
  setValue: React.Dispatch<React.SetStateAction<string[]>>
  data:Result[]
  value:string[]
  setSelectedOption: React.Dispatch<React.SetStateAction<string>>
  selectedOption:string
};

const SearchFilter = ({ data , setValue , value ,selectedOption,setSelectedOption}: Props) => {
  const [array, setArray] = useState<any[]>([]);
  useEffect(() => {
    let arr:any =[];
    data.forEach( i =>arr.push(i.teacher.fullName))
    setArray([...arr])
  }, [])
  const [isOpen, setIsOpen] = useState(false);

  const unique = [...new Set(array.map((item:any) => item))];

   const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

   const handleOptionClick = (item:string) => {
    setSelectedOption(item);
  };
 
  return (
    <div className="relative  text-left hidden lg:inline-block">
      <div>
        <h2 className="mx-5 mt-5 mb-2 font-bold">جستجو بر اساس اساتید</h2>
        <button
          type="button"
          onClick={toggleDropdown}
          className="inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          id="options-menu"
          aria-haspopup="true"
          aria-expanded="true"
        >
          {selectedOption}
        </button>
      </div>

      {isOpen && (
        <div className="origin-top-right absolute right-0 mt-2 w-44 h-28 overflow-x-hidden  rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
          <div
            className="py-1"
            role="menu"
            aria-orientation="vertical"
            aria-labelledby="options-menu"
          >
            <button
              onClick={() => handleOptionClick("انتخاب همه")}
              className="block pr-2 pl-6 py-2 w-44 shadow-sm   text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
              role="menuitem"
            >
              انتخاب همه
            </button>
            {unique.map((item, index) => (
              <button
                key={index}
                onClick={() => handleOptionClick(item)}
                className="block pr-2 pl-6 py-2 w-44 shadow-sm   text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                role="menuitem"
              >
                {item}
              </button>
            ))}
          </div>
        </div>
      )}
    </div>
  );
};

export default SearchFilter;
