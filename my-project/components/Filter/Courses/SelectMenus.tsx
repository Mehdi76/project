import React, { useState } from 'react'

interface props{
    setData: React.Dispatch<React.SetStateAction<{
        src: string;
        courseName: string;
        teacher: string;
        price: string;
        studentCount: number;
        animation: string;
        delay: string;
    }[]>>
    courseData: {
        src: string;
        courseName: string;
        teacher: string;
        price: string;
        studentCount: number;
        animation: string;
        delay: string;
    }[]
}

const SelectMenus = (props:Props) => {
    const [selectedOption, setSelectedOption] = useState("4");
    const uniqueOptions = [
        ...new Set(props.data.map((item) => item.courseNAme)),
      ];

    const toggleDropdown = () => {
        
        setIsOpen(!isOpen);
      };
  return (
    <div>
        <button
    type="button"
    onClick={toggleDropdown}
    className="inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
    id="options-menu"
    aria-haspopup="true"
    aria-expanded="true"
  >
   {selectedOption}
  </button>
</div>
  )
}

export default SelectMenus