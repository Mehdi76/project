/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import * as _ from "lodash";
interface Result {
  title:string
  cost:number
  endDate: string
  startDate: string
  capacity: number
  image: string
  description: string
  teacher: {
      fullName: string
      email: string
      _id: string
      profile: string
  },
  lesson: {
    _id: string
    lessonName: string
    
image:string
topics:string[]
  },
}
interface Props  {
  setChecked: React.Dispatch<React.SetStateAction<string[]>>  | any
  data:Result[]
  checked:string[]
};

const Category = ({setChecked , checked , data}: Props) => {
  const [array, setArray] = useState<any[]>([]);
  useEffect(() => {
    let arr:any =[];
    data.forEach( i => i.lesson.topics.forEach(x=>arr.push(x)))
    setArray([...arr])
  }, [])
  const uniqueOptions = [...new Set(array.map((item:any) => item))];
   const handleOptionClick = (item: string) => {
    let arr = checked 
    if (
  checked.includes(item)
)

{
  const bookMark = checked.findIndex(x => x === item)
 arr.splice(bookMark , 1)
 setChecked([...arr])
}
else {
  setChecked([...checked , item])

}

   }
  return (
    <div className="m-auto grid grid-cols-4">
      <div className="m-4 col-span-4  ">
        <h2 className="text-center mb-2 mt-1 font-bold">دسته بندی</h2>
        <div className="w-full text-sm mb-3 text-gray-900 rounded-lg shadow-lg bg-white grid grid-cols-6 py-2">
          {uniqueOptions.map((item, index) => (
            <ul
              key={index}
              className="  col-span-6 sm:col-span-2 lg:col-span-2 flex items-center px-3"
            >
              <li className="py-3 ml-2 w-full text-sm text-neutral-800 flex ">
                <input
                  type="checkbox"
                   onChange={() => handleOptionClick(item)}
                  className="w-4 h-4 text-neutral-800 ml-3 bg-gray-100 rounded border-gray-300 "
                />
                {item}
              </li>
            </ul>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Category;



