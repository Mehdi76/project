import { Tourney } from 'next/font/google';
import React, { useState } from 'react';

interface Result {
  title:string
  cost:number
  endDate: string
  startDate: string
  capacity: number
  image: string
  description: string
  teacher: {
      fullName: string
      email: string
      _id: string
      profile: string
  },
  lesson: {
    _id: string
    lessonName: string
    
image:string
topics:string[]
  },
}
interface Props  {
  setCost: React.Dispatch<React.SetStateAction<boolean>>
  data:Result[]
  cost:boolean
  free:boolean
  setFree:React.Dispatch<React.SetStateAction<boolean>>
};

 const Price = ({ data , setCost , setFree , cost , free   }: Props) => {

  const handleAllClick = () => {

    setCost(false);
   setFree(false);
  };
  const handlePaidClick = () => {
    setCost(true);
    setFree(false);
  };

  const handleFreeClick = () => {
    setCost(false);
    setFree(true);
 
}
  return (
    <>
    <h2 className='text-center font-bold mt-5'>جستجو بر اساس </h2>
      <div className="m-auto mt-2 py-3 w-24 sm:w-36 bg-white rounded-lg">
      <label className='block'>
          <input
            type="radio"
            checked={!cost && !free}
            onChange={handleAllClick}
            className="inline mr-4 mb-5"
          />
          <span className="pr-1 inline">همه</span>
        </label>
        <label className='block'>
          <input
            type="radio"
            checked={cost}
            onChange={handlePaidClick}
            className="inline mr-4 mb-6"
          />
          <span className="pr-1 inline">خریدنی</span>
        </label>
        <label className='block'>
          <input
            type="radio"
            checked={free}
            onChange={handleFreeClick}
            className="inline mr-4 mb-5"
          />
          <span className="pr-1 inline">رایگان</span>
        </label>
      </div>
    </>
  );
};

export default Price;
