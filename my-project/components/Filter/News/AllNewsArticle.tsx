import React, { useState } from 'react'

 /*  interface Props {
    setState: React.Dispatch<React.SetStateAction<({
        src: string;
        Title: string;
        category: string;
        detail: string;
        date: string;
        animation: string;
        delay: string;
    } | {
        src: string;
        Title: string;
        category: string;
        detail: string;
        date: string;
        animation?: undefined;
        delay?: undefined;
    })[]>>;
    state: ({
        src: string;
        Title: string;
        category: string;
        detail: string;
        date: string;
        animation: string;
        delay: string;
    } | {
        src: string;
        Title: string;
        category: string;
        detail: string;
        date: string;
        animation?: undefined;
        delay?: undefined;
    })[]
  } */
  
interface Props {
    setArticle:React.Dispatch<React.SetStateAction<boolean>>
    setNews :React.Dispatch<React.SetStateAction<boolean>>

  }
  interface Data {
    title: string;
    category: string;
    // other properties
}

const AllNewsArticle = ({setNews,setArticle }: Props) => {
    const handleAllClick = () => {
        setNews(false)
        setArticle(false)
    }
    const handleNewsClick = () => {
        setNews(true)
        setArticle(false)
    }
    const handleArticleClick = () => {
        setNews(false)
        setArticle(true)
    }
    return (
        <div className=" w-48 mt-2 h-[37px] rounded-md m-auto md:mr-10 md:ml-0">
            <button onClick={handleAllClick}/*  ${color ? "bg-blue-900" : "" }  */className={`pl-4 bg-blue-300 border-l border-white h-[37px] pr-4 hover:text-white rounded-lg hover:bg-blue-600`}>همه</button>
            <button onClick={handleNewsClick}/*  style={{background:color?"blue":"white"}} */ className="pl-4 bg-blue-300 border-l border-white h-[37px] pr-4 hover:text-white rounded-md hover:bg-blue-600">اخبار</button>
            <button onClick={handleArticleClick} /* style={{background:color?"blue":"white"}} */ className="pl-4 bg-blue-300 h-[37px] pr-4 hover:bg-blue-600 rounded-md hover:text-white  ">مقالات</button>
        </div>
    )
}

export default AllNewsArticle