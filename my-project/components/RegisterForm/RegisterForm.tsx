import React, { useEffect, useState } from "react";
import Image from "next/image";
import {
  ErrorMessage,
  Field,
  Form,
  Formik,
  FormikProvider,
  useFormik,
} from "formik";
import * as yup from "yup";
import InputForm from "../common/InputForm";
import Button from "../common/Button";
import Picker from "../DatePicker/Picker";
import { RegisterUser } from "@/core/services/api/Auth/Register/register-api";
import Link from "next/link";
import { useRouter } from "next/router";
import eyeicon from "../../public/assets/images/Common/eyeicon.png";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import LoginUser from "@/core/services/api/Auth/Login/login-api";

const validation = yup.object().shape({
  fullName: yup
    .string()
    .required(" لطفا نام خود را وارد کنید ")
    .matches(/^[آ-ی]*$/, " لطفا از الفبای فارسی استفاده کنید"),

  phoneNumber: yup
    .string()
    .required(" لطفا شماره موبایل خود را وارد کنید ")
    .matches(/^09[0|1|2|3|9][0-9]*$/, "11 رقم وارد شود")
    .min(11, "تعداد شماره های وارد شده درست نیست")
    .max(11, "تعداد شماره های وارد شده درست نیست"),

  nationalId: yup
    .string()
    .required(" لطفا کد ملی خود را وارد کنید ")
    .matches(/^[0-9]*$/, "10 رقم وارد شود")
    .min(10, "تعداد ارقام های وارد شده درست نیست")
    .max(10, "تعداد ارقام های وارد شده درست نیست"),

  email: yup
    .string()
    .email("فرمت ایمیل اشتباه می باشد")
    .required(" لطفا ایمیل خود را وارد کنید "),

  password: yup.string().required(" لطفا  پسورد خود را وارد کنید ")
    .matches(
      /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/,
      " رمز عبور باید شامل 8 کاراکتر و 1 حرف بزرگ و 1 حرف کوچک و 1 عدد و 1 کاراکتر خاص باشد"
    ),
});
const RegisterForm = () => {
  const [passwordShown, setPasswordShown] = useState(true);

  const [BirthDayVal, setBirthDayVal] = useState("۱۴۰۲/۰۳/۰۱");
  const route = useRouter();

  const handleSubmit = async (value: {
    fullName: string;
    email: string;
    password: string;
    phoneNumber: string;
    birthDate: string;
    nationalId: string;
  }) => {
    const val = {
      fullName: value.fullName,
      email: value.email,
      password: value.password,
      phoneNumber: value.phoneNumber,
      birthDate: BirthDayVal,
      nationalId: value.nationalId,
      profile:
        "http://res.cloudinary.com/df9w7u89a/image/upload/v1685458512/jpalxotqsjlvctfexa71.png",
    };
    const register = await RegisterUser(val);
    
    if (register) {
      toast.success("با موفقیت ثبت نام کردید");
      route.push("/");      
    }else{
      toast.error("ثبت نام با موفقیت انجام نشد");

    }
  };
  useEffect(() => {
    formikRegister.setFieldValue("birthDate", BirthDayVal);
  }, [BirthDayVal]);

  const formikRegister = useFormik({
    initialValues: {
      fullName: "",
      email: "",
      password: "",
      phoneNumber: "",
      birthDate: "",
      nationalId: "",
    },
    onSubmit: handleSubmit,
    validationSchema: validation,
  });
  return (
    <>
      <div className="w-[500px] h-[480px] border-2 border-sky-500 rounded-xl mt-16">
        <div>
          <div className="-mt-4 mr-6 text-center bg-white w-32">
            <h3 className="text-sky-600 text-xl"> ثبت نام کاربر</h3>
            <div>
              <FormikProvider value={formikRegister}>
                <div className="">
                  <Form className="grid grid-cols-2 gap-52 mt-4">
                    <div className="">
                      <div className="mb-5 h-20">
                        <InputForm
                          widthStyle="192px"
                          stylee="180px"
                          title=" نام کاربر"
                          name="fullName"
                        />
                      </div>
                      <div className="mb-5 h-20">
                        <InputForm
                          widthStyle="192px"
                          stylee="180px"
                          title=" شماره ملی"
                          name="nationalId"
                        />
                      </div>
                      <div className="mb-5 h-20">
                        <InputForm
                          widthStyle="192px"
                          stylee="180px"
                          title=" ایمیل کاربر"
                          name="email"
                        />
                      </div>
                    </div>
                    <div className="">
                      <div className="mr-2 mb-5 w-20 h-20">
                        <label className="text-gray-500 text-sm">
                          تاریخ تولد
                        </label>
                        <Picker val={BirthDayVal} setval={setBirthDayVal} />
                      </div>
                      <div className="mb-5 h-20">
                        <InputForm
                          widthStyle="192px"
                          stylee="180px"
                          title=" شماره تماس"
                          name="phoneNumber"
                        />
                      </div>
                      <div className="mb-5 h-20 ">
                        <InputForm
                          widthStyle="192px"
                          stylee="180px"
                          title=" رمز عبور"
                          name="password"
                        /* types={passwordShown ? "password" : "text"} */
                        />
                        {/* <Image
                          className=" cursor-pointer absolute mr-36 -mt-7   "
                          src={eyeicon}
                          height={30}
                          width={25}
                          alt="none"
                          onClick={() => {
                            setPasswordShown(!passwordShown);
                          }}
                        /> */}
                      </div>
                    </div>
                  </Form>
                </div>
              </FormikProvider>
            </div>

            <br />

            <div className="flex mr-10 w-full">
              <div className="ml-2">
                <Button
                  type="submit"
                  onClick={() => formikRegister.handleSubmit()}
                  title="ثبت نام"
                  class="active:bg-blue-700 hover:bg-blue-400 w-36 p-2  bg-sky-400 text-teal-50 rounded-md shadow-2xl"
                />
                <ToastContainer />

              </div>
              <div>
                <Link href="../Login" >
                  <Button
                    title="ورود"
                    class=" active:bg-green-400 active:text-white w-36 p-2  bg-emerald-50-400 text-green-500 border border-green-500 rounded-md shadow-2xl"
                  />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default RegisterForm;
