import React, { useEffect, useState } from "react";
import * as _ from "lodash";

interface Props {
  setState: React.Dispatch<React.SetStateAction<string>>
  state:string
}

const CoursesSearch = ({state , setState}:Props) => {

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setState(e.target.value);
  };



  return (
    <div className="h-9  sm:mr-3 ">
      <input
        value={state}
        onChange={handleChange}
        type="text"
        className="flex w-48 h-9 bg-gray-50 border border-gray-300 text-gray-900 mb-8 text-sm rounded-lg pl-10 p-2.5 hover:bg-sky-50"
        placeholder="جستجو ..."
      />
      <button
        type="submit"
        className="p-1.5 -mt-[3px] mr-40 relative bottom-16 ml-2 text-sm font-medium text-white bg-blue-400 rounded-lg border border-blue-300 hover:bg-blue-600 "
      >
        <svg
          className="w-5 h-5"
          fill="none"
          stroke="currentColor"
          viewBox="0 0 24 24"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="2"
            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
          ></path>
        </svg>
      </button>
    </div>
  );
};

export default CoursesSearch;
