import Image from "next/image";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as yup from "yup";
import InputForm from "../common/InputForm";
import Button from "../common/Button";
import LoginForm from "../LoginForm/LoginForm";
import Header from "../common/Header";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

const 
LoginPage = () => {

  useEffect(() => {
    AOS.init();
  }, []);
  
  return (
    <>
      <section  className="m-auto">
        <div>
          <div className="relative mt-[-270px]">
            <Header />
          </div>
          <div data-aos="zoom-in" data-aos-duration="2000" className="grid sm:grid-cols-1 md:grid-cols-2  place-items-center relative top-[190px]">
            <LoginForm />

            <div className="sm:hidden md:block hidden">
              <Image
                src="/assets/images/Login/login.png"
                alt="login"
                height={475}
                width={445}
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default LoginPage;



/* import React, { createContext, useState } from "react"

type Template = any
interface IAppContext {
  isMobile: boolean | undefined
  setIsMobile: React.Dispatch<React.SetStateAction<boolean | undefined>>
  templates: Template[]
  setTemplates: (templates: Template[]) => void
  uploads: any[]
  setUploads: (templates: any[]) => void
  shapes: any[]
  setShapes: (templates: any[]) => void
 
  activeSubMenu: string | null
  setActiveSubMenu: (option: string) => void
  currentTemplate: any
  setCurrentTemplate: any
}

export const AppContext = createContext<IAppContext>({
  isMobile: false,
  setIsMobile: () => {},
  templates: [],
  setTemplates: () => {},
  uploads: [],
  setUploads: () => {},
  shapes: [],
  setShapes: () => {},

 
  activeSubMenu: null,
  setActiveSubMenu: (value: string) => {},
  currentTemplate: {},
  setCurrentTemplate: {},
})

export const AppProvider = ({ children }: { children: React.ReactNode }) => {
  const [isMobile, setIsMobile] = useState<boolean | undefined>(undefined)
  const [templates, setTemplates] = useState<Template[]>([])
  const [uploads, setUploads] = useState<any[]>([])
  const [shapes, setShapes] = useState<Template[]>([])

  const [activeSubMenu, setActiveSubMenu] = useState<string | null>(null)
  const [currentTemplate, setCurrentTemplate] = useState(null)
  const context = {
    isMobile,
    setIsMobile,
    templates,
    setTemplates,
    activePanel,
    setActivePanel,
    shapes,
    setShapes,
    activeSubMenu,
    setActiveSubMenu,
    uploads,
    setUploads,
    currentTemplate,
    setCurrentTemplate,
  }
  return <AppContext.Provider value={context}>{children}</AppContext.Provider>
} */