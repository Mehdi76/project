import React from "react";
interface Props {
  title: string;
  style: string;
}
const Title = (props: Props) => {
  return (
    <div className=" text-center">
      <h2 className={props.style}>{props.title}</h2>
    </div>
  );
};

export default Title;
