import React from 'react'
 interface props{
  Title:string
 }
 
const NewsTitle = (props:props) => {
  return (
    <div className=' max-w-7xl text-center text-blue-900 text-3xl mt-20 mr-5 border-r-4 border-yellow-300 inline-block px-4 '>{props.Title}</div>
  )
}

export default NewsTitle