import * as React from "react";

interface Props {
  title: string;
  class: string;
  type?: "button" | "submit" | "reset" | undefined
  onClick?:React.MouseEventHandler<HTMLButtonElement> | undefined
}

const Button = (props: Props) => {
  return (
    <>
      <button type={props.type} onClick={props.onClick} className={props.class}>{props.title}</button>
    </>
  );
};

export default Button;
