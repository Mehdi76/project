import React, { useEffect } from 'react'
import NewsTitle from './NewsTitle'
import ReviewsDetail from './ReviewsDetail'
import AOS from 'aos';
import 'aos/dist/aos.css';

const data = [{
  src: "/assets/images/NewsDetail/User.png",
  name: "ایرج صدر",
  date: "15 اردیبهشت 1402",
  review: "این موضوع انقلاب بزرگی در وقایع آتی در دنیای فناوری و غیر فناوری به وجود خواهد آورد .",
  revieww: ".نسل بعد ما را قضاوت خواهند کرد"
},

{
  src: "/assets/images/NewsDetail/User1.png",
  name: "مارال فرمانیان",
  date: "16 اردیبهشت 1402",
  review: "این موضوع انقلاب بزرگی در وقایع آتی در دنیای فناوری و غیر فناوری به وجود خواهد آورد."
}
]
const Reviews = () => {
  useEffect(() => {
    AOS.init()
}, [])
  return (
    <div className='max-w-7xl'  data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine" data-aos-duration="1500">
      <div className='m-auto'>
        <NewsTitle Title=" نظرات کاربران"></NewsTitle>
      </div>
      <div className='bg-blue-200 h-fit rounded-lg w-fit px-7 mt-12 m-auto'>
        <h2 className=' text-lg leading-10 '>تنها کاربران سایت قادر به ثبت نظر هستند. برای ثبت نظر لازم است تا ثبت‌نام نمایید و یا وارد شوید </h2>
      </div>
      <div className='mt-12 '>
        {data.map((item, index) => (
          <ReviewsDetail
            key={index}
            name={item.name}
            src={item.src}
            date={item.date}
            review={item.review}
            revieww={item.revieww}

          />
        ))}
      </div>

    </div>


  )
}

export default Reviews
