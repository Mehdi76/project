import Image from "next/image";
import React, { useEffect, useState } from "react";
import Hamber from "../Hamber/Hamber";
import Link from "next/link";
import { useTheme } from "next-themes";
import Cookie from "js-cookie";
import { useContext } from "react";
import { HistoryContext } from "@/core/context/HistoryContext";
import { eraseCookie } from "@/core/services/storage/cookies.storage";

const headerItem = [
  { title: "خانه", href: "../Landing" },
  { title: "دوره ها", href: "../Courses" },
  { title: "سوالات", href: "../Faq" },
  { title: "مقالات", href: "../News" },
  { title: "ارتباط با ما", href: "../Contact" },
];

const Header = () => {
  const { history } = useContext(HistoryContext);
  const [logUser, setLogUser] = useState(false);
  const [show, setShow] = useState(true);
  const { theme, setTheme } = useTheme();
  const [mounted, setMounted] = useState(false);
  const [sunShown, setSunShown] = useState(theme === "light");
  const [moonShown, setMoonShown] = useState(theme === "dark");
  // useEffect(() => setMounted(true), []);
  // if (!mounted) return null;

  const handleLogOut = () => {
    Cookie.remove("token");
    Cookie.remove("user");
    setLogUser(false);
  };

  // useEffect(() => {
  //   console.log(logUser);
  //   if (!logUser) {
  //     Cookie.remove("token");
  //   }
  // }, [logUser]);

  useEffect(() => {
    const token = Cookie.get("token");
    if (token) setLogUser(true);
  }, []);

  return (
    <div className="mb-36">
      <section className="mx-auto  w-[95%] right-0 left-0  top-1 fixed h-20 z-20 backdrop-blur-md bg-opacity-60 dark:bg-slate-700 dark:backdrop-blur-md dark:bg-opacity-60  rounded-lg mt-1     ">
        <div className="flex flex-row rtl max-w-7xl m-auto relative mb-24">
          <div className=" ml-auto mr-24 mt-1 relative">
            <Link href="../Landing">
              <Image
                src="/assets/images/Landing/logo.png"
                className="flex ml-12 mt-3 lg:mt-0"
                alt="logo"
                height={50}
                width={50}
              />

              <p className="text-blue-900 dark:text-white font-bold mt-1 -mr-10 sm:hidden  lg:block hidden">
                آکادمی کد نویسی بحر
              </p>
            </Link>
            <div className="absolute right-[90px] -top-4">
              <Hamber show1="hidden" showHandler={show} setHandler={setShow} />
            </div>
          </div>

          <ul className="hidden  lg:flex w-[700px] ">
            {headerItem.map((item, index) => (
              <li
                key={index}
                className=" w-auto md:mr-6 mt-7 hover:border-b-2 border-blue-700 cursor-pointer text-lg text-black dark:text-white"
              >
                <Link href={item.href}>{item.title}</Link>
              </li>
            ))}
            <li className="w-24 pr-8 pt-7">
              <button
                onClick={() => {
                  setTheme(theme === "dark" ? "light" : "dark");
                  setSunShown(!sunShown);
                  setMoonShown(!moonShown);
                }}
                className=" shadow-xl mr-4 w-11 h-11 -mt-2 pr-2 rounded-3xl bg-slate-600 dark:bg-yellow-300   "
              >
                {sunShown && (
                  <Image
                    src="/assets/images/Header/moon.svg"
                    alt="sun icon"
                    width={40}
                    height={20}
                    className="pl-2"
                  />
                )}
                {moonShown && (
                  <Image
                    src="/assets/images/Header/sun.svg"
                    alt="moon icon"
                    width={50}
                    height={20}
                    className=" pl-2"
                  />
                )}
              </button>
            </li>
            <li className="w-10 pt-7 dark:text-white"></li>
          </ul>

          <ul
            className={`flex flex-col ml-auto lg:hidden bg-slate-100 pl-9 pb-12  ${
              show && "hidden"
            } mt-6 items-center sm:text-right w-44 font-bold text-xl absolute top-12 right-32 shadow-xl bg-white  lg:relative lg:flex flex-row sm:${
              show && "hidden"
            }`}
          >
            {headerItem.map((item, index) => (
              <li
                className="md:mr-8 mt-12 hover:border-b-2 border-blue-950 cursor-pointer"
                key={index}
              >
                <Link href={item.href}>{item.title}</Link>
              </li>
            ))}
          </ul>

          <div className="flex sm:ml-12 ml-0">
            {!logUser && (
              <div className="h-10 w-10 relative ml-4  mt-5 hover:cursor-pointer">
                <Link href="../Login">
                  <Image
                    src="/assets/images/Landing/account.png"
                    alt="account"
                    layout="fill"
                  />
                </Link>
              </div>
            )}
            {logUser && (
              <>
                <div className="h-10 w-10 relative ml-4 mt-5 hover:cursor-pointer bg-blue-400 rounded-full p-2">
                  <Link href="../StudentPanel/Dashboard">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      className="w-6 h-6 text-white"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M4.26 10.147a60.436 60.436 0 00-.491 6.347A48.627 48.627 0 0112 20.904a48.627 48.627 0 018.232-4.41 60.46 60.46 0 00-.491-6.347m-15.482 0a50.57 50.57 0 00-2.658-.813A59.905 59.905 0 0112 3.493a59.902 59.902 0 0110.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.697 50.697 0 0112 13.489a50.702 50.702 0 017.74-3.342M6.75 15a.75.75 0 100-1.5.75.75 0 000 1.5zm0 0v-3.675A55.378 55.378 0 0112 8.443m-7.007 11.55A5.981 5.981 0 006.75 15.75v-1.5"
                      />
                    </svg>
                  </Link>
                </div>
                <div className="h-10 w-10 relative ml-4 mt-5 hover:cursor-pointer bg-blue-400 rounded-full p-2">
                  <button onClick={() => handleLogOut()}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      className="w-6 h-6 text-white"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75"
                      />
                    </svg>
                  </button>
                </div>
              </>
            )}

            <Link href={"../ShopingCard"}>
              <div className="h-10 w-10 relative ml-4 mt-5 hover:cursor-pointer bg-blue-400 rounded-full p-2">
                <span className="bg-red-500 text-white w-5 h-5 pr-[6px] absolute -top-1 -right-1  rounded-full">
                  {history.length}
                </span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke-width="1.5"
                  stroke="currentColor"
                  className="text-white w-6 h-6"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z"
                  />
                </svg>
              </div>
            </Link>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Header;
