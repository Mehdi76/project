
import { Calendar } from "react-multi-date-picker" 
import persian from "react-date-object/calendars/persian" 
import persian_fa from "react-date-object/locales/persian_fa" 

interface props{
    readOnly:boolean
}


export default function CalenderCustom(props:props) { 
    const weekDays = ["ش", "ی", "د", "س", "چ", "پ", "ج"] 
    return ( 
        <Calendar 
            value={new Date()} 
            calendar={persian} 
            locale={persian_fa} 
            readOnly={props.readOnly} 
            weekDays={weekDays} 
            style={{borderRadius:"16px" , paddingLeft:"8px"}}
        /> 
    ) 
}