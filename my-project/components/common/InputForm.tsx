import { ErrorMessage, Field } from "formik";
import Image from "next/image";
import React from "react";
import eyeicon from "../../public/assets/images/Common/eyeicon.png";

interface Props {
  title: string;
  name: string;
  stylee?: string;
  class?: string;
  widthStyle?: string;
  marginStyle?: string;
  marginStylee?: string;
  list?: string;
  style?: string;
  types?: string;
}

const InputForm = (props: Props) => {
  return (
    <>
      <div className="w-20 relative">
        <label
          className="text-gray-500 text-sm relative left-4 mr-3"
          style={{
            marginLeft: props.marginStyle,
            marginRight: props.marginStylee,
          }}
          htmlFor=""
        >
          {props.title}
        </label>
        <Field
          style={{ width: props.widthStyle }}
          name={props.name}
          className="relative border border-gray-300 rounded-lg text-gray-950 w-72 h-9"
          type={props.types}
        />
        <ErrorMessage
          name={props.name}
          render={(msg) => (
            <p style={{ color: "red", fontSize: "14px", marginLeft: "64px" , width:"200px" }}>
              {msg}
            </p>
          )}
        />

      </div>
     
    </>
  );
};

export default InputForm;
