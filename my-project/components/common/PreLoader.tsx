
import React, { useEffect } from "react";

const Preloader = () => {
  useEffect(() => {
    setTimeout(() => {
      const body = document.getElementsByTagName("body")[0];
      body.classList.add("loaded");
    }, 3000);
  }, []);

  return (
    <div id="prelaoder">
      <video autoPlay muted  >
        <source src="/assets/images/shot.mp4" type="video/mp4" />
        Your browser does not support html video.
      </video>
    </div>
  );
};

export default Preloader;
