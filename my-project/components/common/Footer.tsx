import Image from "next/image";
import style from "../../styles/Landing.module.css";
import Link from "next/link";

const pics = [
  { src: "/assets/images/Landing/whatsapp.png" },
  { src: "/assets/images/Landing/twitter.png" },
  { src: "/assets/images/Landing/instagram.png" },
  { src: "/assets/images/Landing/linkedin.png" },
];

const footerItem = [
  { title: "خانه", href: "./Landing" },
  { title: " دوره ها", href: "./Courses" },
  { title: "مقالات", href: "./News" },
  { title: "ارتباط با ما", href: "./Contact" },
  { title: "سوالات", href: "./Faq" },
];

const Footer = () => {
  return (
    <>
      <div className="grid md:grid-cols-3 mt-20 mb-24 sm:grid-cols-1 place-items-center ">
        <div>
          <div className="lg:flex mb-4 mt- sm:hidden hidden">
            {pics.map((item, index) => (
              <div key={index} className="mr-2">
                <Image src={item.src} alt="no" height={50} width={50} />
              </div>
            ))}
          </div>
        </div>
        <div className="mt-12">
          <ul className="flex mt-14 items-center md:mr-12 lg:mr-16 text-blue-800 md:flex md:w-[500px]">
            {footerItem.map((item, index) => (
              <li
                className="mr-7 hover:border-b-2 border-blue-950 cursor-pointer h-7"
                key={index}
              >
                <Link className="dark:text-white" href={item.href}>{item.title}</Link>
              </li>
            ))}
          </ul>
          <div className="flex justify-center mt-20 text-gray-400 text-xl">
            <p>Developed By Logic Team</p>
          </div>
        </div>

        <div className="mt-2 ml-40 sm:hidden lg:flex hidden">
          <Image
            src="/assets/images/Landing/team.png"
            alt="team"
            width={100}
            height={100}
          />
        </div>
      </div>
    </>
  );
};

export default Footer;
