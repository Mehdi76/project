import React from "react";
import Header from "../common/Header";
import Footer from "../common/Footer";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";
const Faqs = () => {
   useEffect(() => {
    AOS.init();
  }, []);
  return (
    <>
      <Header />
      <div className="p-8 -mt-6">
        <div data-aos="fade-left">
        <h4 className="text-4xl font-bold text-gray-900  tracking-widest uppercase text-center">
          سوالات متدوال
        </h4>
        <p className="text-center text-gray-600 text-sm mt-2 mb-16">
          در اینجا سعی می‌کنیم به سوالات متداولی که ممکن است، برای شما در اینجا
          پیش بیاید، پاسخ دهیم.
        </p>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4 xl:gap-12 px-2 xl:px-12 mt-4">
          <div  data-aos-duration="3000" data-aos="fade-left" className="flex space-x-8 mt-8">
            <div className="-mt-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-12 w-12 text-blue-600"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path d="M12 14l9-5-9-5-9 5 9 5z"></path>
                <path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z"></path>
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"
                ></path>
              </svg>
            </div>
            <div>
              <h4 className="text-xl font-bold text-gray-700">
                چطور می‌تونیم دوره‌های آموزشی سایت رو دریافت کنیم؟
              </h4>
              <p className="text-gray-600 my-2">
                دوره‌های نقدی برای دسترسی به دوره‌های نقدی، شما نیاز است ابتدا
                از طریق صفحه عضویت وبسایت، در سایت عضو شوید در قدم بعدی به صفحه
                دوره مورد نظر خود رفته و آن دوره را را به شکل نقدی تهیه کنید، از
                زمانی که شما هزینه دوره را پرداخت کنید می‌توانید به تمام جلسات
                دوره و همینطور آپدیت‌های آینده دوره به شکل کامل دسترسی پیدا کنید
                چه به شکل آنلاین و چه به شکل آفلاین.دوره‌های نقدی را دریافت
                کنید.
              </p>
              <a
                href="#"
                className="text-blue-600 hover:text-blue-800 hover:underline capitalize"
                title="Read More"
              >
                جزئیات بیشتر{" "}
              </a>
            </div>
          </div>

          <div  data-aos-duration="3000" data-aos="fade-left" className="flex space-x-8 mt-8">
            <div className="-mt-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-12 w-12 text-blue-600"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"
                ></path>
              </svg>
            </div>
            <div>
              <h4 className="text-xl font-bold text-gray-700">
                کاربران ویژه به چه فایل‌های دسترسی دارند؟
              </h4>
              <p className="text-gray-600 my-2">
                با توجه به توضیحاتی که در بالا برای دوره‌های ویژه دادیم، اگر شما
                عضویت ویژه را تهیه کرده باشید میتوانید در زمان مشخص شده برای
                اکانت خود، ویدیوهای عضویت ویژه را تنها به شکل آنلاین مشاهده
                فرمایید. نکته مهم :اعضای ویژه همچنین به فایل‌های پیوست هر جلسه
                دسترسی دارند، این فایل‌ها معمولا شامل کد‌های است که در هر جلسه
                نوشته می‌شود.
              </p>
              <a
                href="#"
                className="text-blue-600 hover:text-blue-800 hover:underline capitalize"
                title="Read More"
              >
                جزئیات بیشتر{" "}
              </a>
            </div>
          </div>

          <div  data-aos-duration="3000" data-aos="fade-left" className="flex space-x-8 mt-8">
            <div className="-mt-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-12 w-12 text-blue-600"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M9 3v2m6-2v2M9 19v2m6-2v2M5 9H3m2 6H3m18-6h-2m2 6h-2M7 19h10a2 2 0 002-2V7a2 2 0 00-2-2H7a2 2 0 00-2 2v10a2 2 0 002 2zM9 9h6v6H9V9z"
                ></path>
              </svg>
            </div>
            <div>
              <h4 className="text-xl font-bold text-gray-700">
                چقدر زمان میبرد که اپلیکیشن من آماده شود؟
              </h4>
              <p className="text-gray-600 my-2">
                پاسخ: ایجاد پنل بصورت آنی خواهد بود (در نسخه های نامحدود پنل
                باید طبق آموزش بروی هاست شما نصب شود که حدود 10 دقیقه زمان
                میبرد) و درخواست تحویل فایل apk یا aab بلافاصله بعد از خرید در
                پنل my.approcket.ir برای شما فعال میشود که خروجی اپ شما توسط
                سرور اتوماتیک تولید خروجی در زمانی کمتر از 20 دقیقه تولید خواهد
                شد.
              </p>
              <a
                href="#"
                className="text-blue-600 hover:text-blue-800 hover:underline capitalize"
                title="Read More"
              >
                جزئیات بیشتر{" "}
              </a>
            </div>
          </div>

          <div  data-aos-duration="3000" data-aos="fade-left" className="flex space-x-8 mt-8">
            <div className="-mt-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-12 w-12 text-blue-600"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M7 21a4 4 0 01-4-4V5a2 2 0 012-2h4a2 2 0 012 2v12a4 4 0 01-4 4zm0 0h12a2 2 0 002-2v-4a2 2 0 00-2-2h-2.343M11 7.343l1.657-1.657a2 2 0 012.828 0l2.829 2.829a2 2 0 010 2.828l-8.486 8.485M7 17h.01"
                ></path>
              </svg>
            </div>
            <div>
              <h4 className="text-xl font-bold text-gray-700">
                در بازکردن فایل‌های فشرده مشکل دارم‌!
              </h4>
              <p className="text-gray-600 my-2">
                دانلود ناقص فایل این احتمال وجود دارد که فایل دانلودی شما به شکل
                ناقص دانلود شده باشد. که در این صورت نیاز است دوباره فایل مورد
                نظر را دانلود کنید.
              </p>
              <a
                href="#"
                className="text-blue-600 hover:text-blue-800 hover:underline capitalize"
                title="Read More"
              >
                جزئیات بیشتر{" "}
              </a>
            </div>
          </div>

          <div  data-aos-duration="3000" data-aos="fade-left" className="flex space-x-8 mt-8">
            <div className="-mt-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-12 w-12 text-blue-600"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M4 7v10c0 2.21 3.582 4 8 4s8-1.79 8-4V7M4 7c0 2.21 3.582 4 8 4s8-1.79 8-4M4 7c0-2.21 3.582-4 8-4s8 1.79 8 4m0 5c0 2.21-3.582 4-8 4s-8-1.79-8-4"
                ></path>
              </svg>
            </div>
            <div>
              <h4 className="text-xl font-bold text-gray-700">
                مراحل و فرایند گارانتی چگونه است؟
              </h4>
              <p className="text-gray-600 my-2">
                برای آنکه به شما اطمینان دهیم، که ما از محتوای دوره‌های خود ۱۰۰
                درصد مطمئن هستیم، برای این دوره گارانتی بازگشت وجه قرار داده‌ایم
                و این به این معنی است که اگر شما محتوای این دوره را به شکل کامل
                مشاهده کنید، اما نتیجه‌ای که به شما قول دادیم را دریافت نکنید
                ۱۰۰ درصد مبلغ پرداختی شما را برگشت خواهیم زد. ما در تلاشیم کیفیت
                کار خود را هر روز بهتر از روز قبل کنیم و آموزش‌های را در اختیار
                شما قرار دهیم که لایقشان هستید.
              </p>
              <a
                href="#"
                className="text-blue-600 hover:text-blue-800 hover:underline capitalize"
                title="Read More"
              >
                جزئیات بیشتر{" "}
              </a>
            </div>
          </div>

          <div  data-aos-duration="3000" data-aos="fade-left" className="flex space-x-8 mt-8">
            <div className="-mt-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-12 w-12 text-blue-600"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M7 4v16M17 4v16M3 8h4m10 0h4M3 12h18M3 16h4m10 0h4M4 20h16a1 1 0 001-1V5a1 1 0 00-1-1H4a1 1 0 00-1 1v14a1 1 0 001 1z"
                ></path>
              </svg>
            </div>
            <div>
              <h4 className="text-xl font-bold text-gray-700">
                تجربه کاربری چیست؟
              </h4>
              <p className="text-gray-600 my-2">
                به اساس میزان فعالیت های که کاربر بر روی سایت انجام میدهد مقداری
                تجربه در وبسایت بر اساس نوع فعالیت که از طرف ما مشخص شده کسب
                میکند که این مورد در اطلاعات کاربر ثبت میشود . ما امکانی را
                بوجود اوردیم تا شخص بتواند با استفاده از اعتباری که برای مثال با
                فعالیت در بخش بحث و گفتگو بدست میاورد راحت تر دوره های نقدی سایت
                را خریداری کند
              </p>
              <a
                href="#"
                className="text-blue-600 hover:text-blue-800 hover:underline capitalize"
                title="Read More"
              >
                {" "}
                جزئیات بیشتر{" "}
              </a>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Faqs;
