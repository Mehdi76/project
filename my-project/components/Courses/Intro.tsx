import Image from 'next/image'
import React, { useEffect } from 'react'
import Title from '../common/Title'
import AOS from 'aos';
import 'aos/dist/aos.css';
const Intro = () => {
  
  useEffect(() => {
    AOS.init()
}, [])
  return (
    <nav data-aos="zoom-in-down" data-aos-duration="1500" className="md:grid grid-cols-2 h-96 mx-auto mt-20 xl:mt-14 text-center md:mb-20" style={{ width: "85%" }}>
      <div className="mt-15 m-auto">
          <Title title="دوره‌های برنامه نویسی آموزشگاه" style=" text-blue-900 font-bold text-4xl mb-2 pb-4 mt-6" />
          <h2 className=' text-xl text-gray-600 pr-2 leading-loose' >بیش از ده‌ها دوره‌ آموزشی مطابق با بهترین و جدیدترین
            <p className="text-xl text-gray-600 leading-loose">متدهای روز دنیا </p>  </h2>
        </div>
        <div>
            <Image src="/assets/images/Courses/Courses.PNG"
              alt='courses'
              height={450}
              width={450}
              className='md:flex sm:mr-10 m-auto hidden  '
            />
        </div>
      </nav>
  )
}

export default Intro