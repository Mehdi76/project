import Header from '@/components/common/Header'
import Footer from '@/components/common/Footer'
import React from 'react'
import CourseCard from "@/components/Courses/CourseCard";
import Title from '@/components/common/Title';
import Intro from '@/components/Courses/Intro';

interface Result {
  title:string
  cost:number
  endDate: string
  startDate: string
  capacity: number
  image: string
  description: string
  teacher: {
      fullName: string
      email: string
      _id: string
      profile: string
  },
  lesson: {
    _id: string
    lessonName: string
    topics:string[],

},
}
type Props={
data:Result[]
}
const CoursesFinal = ({data}:Props) => {
  return (
    <>
    <Header />
      <Intro />
      <Title title="لیست تمام دوره ها" style="text-center text-blue-950 text-4xl mb-5 mt-6 border-r-4 border-yellow-300 inline-block px-4 w-72" />
      <CourseCard data={data}/>
      <Footer />
    </>
  )
}

export default CoursesFinal