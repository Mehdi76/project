import React, { useEffect } from "react";
import { useState } from "react";
import Image from "next/image";
import PaginationNav1 from "../common/Pagination";
import Button from "../common/Button";
import { useRouter } from "next/router";
import * as _ from "lodash";
import AOS from "aos";
import "aos/dist/aos.css";
import CoursesSearch from "../Search/CoursesSearch";
import CoursesFilter from "../Filter/Courses/CoursesFilter";
import Category from "../Filter/Courses/Category";
import SearchFilter from "../Filter/Courses/SearchFilter";
import Price from "../Filter/Courses/Price";

interface Result {
  _id: string;
  title: string;
  cost: number;
  endDate: string;
  startDate: string;
  capacity: number;
  image: string;
  description: string;
  teacher: {
    fullName: string;
    email: string;
    _id: string;
    profile: string;
  };
  lesson: {
    _id: string;
    lessonName: string;
    topics: string[];
    image: string;
  };
}

const CourseCard = ({ data }: { data: Result[] }) => {

  const router = useRouter();
  const [view, setView] = useState(false);
  const [checked, setChecked] = useState<string[]>([]);
  const [show, setShow] = useState(false);
  const [free, setFree] = useState(false);
  const [tea, setTea] = useState();
  const [value, setValue] = useState([""]);
  const [selectedOption, setSelectedOption] = useState("انتخاب همه");

  const [cost, setCost] = useState(false);

  const [pageIndex, setPageIndex] = useState(0);
  const pageCount = data.length / 4;

  const [state, setState] = useState("");
  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <div className=" relative w-full mt-12">
      <div className="grid w-[78%] grid-cols-12 m-auto   ">
        <div className="col-span-11  ">
          <CoursesSearch state={state} setState={setState} />
        </div>

        <div>
          <div onClick={() => setShow(!show)} className=" col-span-1 w-9 ">
            <CoursesFilter view={view} setView={setView} />
          </div>
        </div>
        {show && (
          <div
            data-aos="fade-down"
            data-aos-duration="1000"
            className={`w-[100%] m-auto bg-blue-100 mt-1 h-68 rounded-lg shadow-lg z-10 col-span-12 `}
          >
            <div className="grid lg:grid-cols-4 grid-cols-3 ">
              <div className="col-span-2">
                <Category data={data} setChecked={setChecked} checked={checked} />
              </div>
              <div className="col-span-1  ">
                <Price
                  data={data}
                  setCost={setCost}
                  cost={cost}
                  free={free}
                  setFree={setFree}
                />
              </div>
              <div className="col-span-1 ">
                <SearchFilter
                  selectedOption={selectedOption}
                  setSelectedOption={setSelectedOption}
                  data={data}
                  value={value}
                  setValue={setValue}
                />
              </div>
            </div>
          </div>
        )}
      </div>
      <div className=" relative grid xl:grid-cols-4 lg:grid-cols-3 sm:grid-cols-2 w-fit mx-auto mt-5  ">
        {data
          .filter(
            (item: {
              title: string;
              cost: number;
              teacher: {
                fullName: string;
                email: string;
                _id: string;
                profile: string;
              };
              lesson: {
                _id: string;
                lessonName: string;
                topics: string[];
                image: string;
              };
            }) =>
              _.startsWith(item.title, state) &&
              (cost ? item.cost !== 0 : free ? item.cost === 0 : item.cost) &&
              (selectedOption === "انتخاب همه"
                ? item.teacher.fullName
                : item.teacher.fullName === selectedOption) && 
                checked.length === 0 ? item : 
              checked.some(x => item.lesson.topics.includes(x) )
          )
          .slice((pageIndex + 1) * 4 - 4, (pageIndex + 1) * 4)
          .map((item, index) => (
            <div
              key={index}
              className="max-w-sm m-2  rounded-xl overflow-hidden shadow-lg h-80 w-52 grid grid-rows-2"
            >
              <div className="flex relative items-center justify-center bg-blue-100">
                <div className="w-5 h-5 absolute top-1 left-1 text-xs ml-1 text-center ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    className=""
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z"
                    />
                  </svg>
                  {item.capacity}
                </div>
                <Image
                  src={item.lesson.image}
                  alt="card"
                  width={155}
                  height={150}
                />
              </div>
              <div className="px-3 py-1">
                <div className=" text-2xl mb-2 text-center text-blue-900">
                  {item.title}
                </div>
                <div className=" flex justify-between text-xs mt-4  ">
                  <div className="flex">
                    <div className="flex">
                      <svg
                        id="Icons_Teacher"
                        overflow="hidden"
                        width={20}
                        version="1.1"
                        viewBox="0 0 96 96"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        {" "}
                        <path d=" M 87.8 19 L 23.8 19 C 21.6 19 19.8 20.8 19.8 23 L 19.8 37.5 C 20.9 37.2 22.2 37 23.4 37 C 24.2 37 25 37.1 25.8 37.2 L 25.8 25 L 85.8 25 L 85.8 63 L 51.9 63 L 46.2 69 L 87.8 69 C 90 69 91.8 67.2 91.8 65 L 91.8 23 C 91.8 20.8 90 19 87.8 19" />{" "}
                        <path d=" M 23.5 58 C 28.2 58 32 54.2 32 49.5 C 32 44.8 28.2 41 23.5 41 C 18.8 41 15 44.8 15 49.5 C 14.9 54.2 18.8 58 23.5 58" />{" "}
                        <path d=" M 56.2 48.1 C 54.9 46.1 52.3 45.6 50.3 46.8 C 49.9 47 49.7 47.4 49.5 47.6 L 34.9 62.8 C 33.5 62.1 32 61.5 30.5 61 C 28.2 60.6 25.8 60.1 23.5 60.1 C 21.2 60.1 18.8 60.5 16.5 61.2 C 13.1 62.1 10.1 63.8 7.6 65.9 C 7 66.5 6.5 67.4 6.3 68.2 L 4.2 77 L 34.1 77 L 34.1 76.9 L 42.6 67 L 55.7 53.2 C 56.9 52 57.3 49.7 56.2 48.1" />
                      </svg>
                    </div>
                    <p>{item?.teacher?.fullName}</p>
                  </div>
                  <div className="text-red-500">{item.cost} </div>
                </div>
                <Button
                  onClick={() =>
                    router.pathname === "/Courses/[newId]"
                      ? router.push(`${item._id}`)
                      : router.push(`Courses/${item._id}`)
                  }
                  title="نمایش جزئیات"
                  class="active:bg-blue-800 active:text-white  flex justify-center  text-white mx-auto w-36 h-19 p-2 rounded-lg mt-8  hover:shadow-xl bg-blue-500 hover:bg-blue-600  shadow-blue-500/50"
                />
              </div>
            </div>
          ))}
        <div className="w-13 sm:block hidden absolute -right-14 top-7 "></div>
      </div>
      <div className="py-2">
        <PaginationNav1
          gotoPage={setPageIndex}
          canPreviousPage={pageIndex > 0}
          canNextPage={pageIndex < pageCount - 1}
          pageCount={pageCount}
          pageIndex={pageIndex}
        />
      </div>
    </div>
  );
};

export default CourseCard;
