import CourseCard from '@/components/Courses/CourseCard'
import Description from '@/components/CoursesDetail/Description'
import Intro from '@/components/CoursesDetail/Intro'
import NewsTitle from '@/components/common/NewsTitle'
import Reviews from '@/components/common/Reviews'
import Header from '@/components/common/Header'
import React from 'react'
import Footer from '@/components/common/Footer'


interface Props {
  data:{
  title: string
  cost: number
  endDate: string
  startDate: string
  capacity: number
  image: string
  description: string
  teacher: {
    fullName: string
    email: string
    _id: string
    profile: string
  },
  lesson: {
    _id: string
    lessonName: string
    topics: string[],
    image: string
  },
}
allData: {
  _id: string;
title: string;
cost: number;
endDate: string;
startDate: string;
capacity: number;
image: string;
description: string;
teacher: {
    fullName: string;
    email: string;
    _id: string;
    profile: string;
};
lesson: {
    _id: string;
    lessonName: string;
    topics: string[];
    image: string;
};
}[]
}

const CoursesDetailFinal = ({data , allData}:Props) => {
  return (
    <>
        <Header />
        <div className='sm:mx-16 mx-7' >
        <Intro data={data}/>
        <Description data={data}/>
        <NewsTitle Title='دوره های مرتبط'></NewsTitle>
         <CourseCard data={allData}/> 
        <Reviews />
        </div>
        <Footer/>
    </>
  )
}

export default CoursesDetailFinal