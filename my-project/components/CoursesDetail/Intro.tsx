import React, { useEffect, useState } from "react";
import Title from "../common/Title";
import Image from "next/image";
import AOS from "aos";
import "aos/dist/aos.css";
import Cookie from "js-cookie";
import { useRouter } from "next/router";
import { useContext } from "react";
import { HistoryContext } from "@/core/context/HistoryContext";

interface Props {
  data: {
    title: string;
    cost: number;
    endDate: string;
    startDate: string;
    capacity: number;
    image: string;
    _id: string;
    description: string;
    teacher: {
      fullName: string;
      email: string;
      _id: string;
      profile: string;
    };
    lesson: {
      _id: string;
      lessonName: string;
      topics: string[];
      image: string;
    };
  };
}
/* const data = {
    teacher: " مدرس : ایرج صدر ",
    time:" مدت دوره : 5 ساعت (14 ویدئو) ",
    capacity:" ظرفیت دوره : 40 نفر",
    student:" دانشجویان دوره : 20 نفر",
    start:" شروع: 15 اردیبهشت 1402",
    cost:"شهریه دوره: 150.000 تومان",
    src:"/assets/images/CoursesDetail/html5.png",
} */

const Intro = ({ data }: Props) => {
  const { history, handleHistory, handleDelete } = useContext(HistoryContext);
  // handleId(data.title);
  console.log(data);
  const [shoppingCardArray, setShoppingCardArray] = useState([]);
  const [isIncludeCourse, setIsIncludeCourse] = useState(false);
  // setShoppingCardArray({
  //   title: data.title,
  //   startDate: data.startDate,
  //   cost: data.cost,
  // });

  const router = useRouter();
  const [logUser, setLogUser] = useState(false);

  // useEffect(() => {
  //   const title = data.title;
  //   const startDate = data.startDate;
  //   const cost = data.cost;
  //   const array = { title, startDate, cost };
  //   handleHistory(array);
  // }, []);

  useEffect(() => {
    AOS.init();
  }, []);

  useEffect(() => {
    const token = Cookie.get("token");
    if (token) setLogUser(true);
  }, []);

  useEffect(() => {
    setIsIncludeCourse(false);
    const result: any = history.filter(
      (item: any) => item.courseId === data._id
    );
    if (result.length) {
      setIsIncludeCourse(true);
    } else {
      setIsIncludeCourse(false);
    }
    console.log("history", history);
  }, [history, data]);

  useEffect(() => {
    console.log(isIncludeCourse);
  }, [isIncludeCourse]);

  const handleShopping = (e: any) => {
    e.preventDefault();
    const array = {
      courseId: data._id,
      title: data.title,
      startDate: data.startDate,
      cost: data.cost,
    };
    handleHistory(array);
  };

  const handleDeleteHistory = (e: any) => {
    e.preventDefault();
    handleDelete(data._id);
  };

  return (
    <div
      className="max-w-7xl m-auto "
      data-aos="zoom-in-down"
      data-aos-duration="1500"
    >
      <div className="grid lg:grid-cols-2 mt-12 max-w-7xl m-auto ">
        <div className="grid lg:grid-cols-2 w-11/12 lg:mr-20  mt-36 text-gray-500  ">
          <div className=" lg:col-span-2 w-96 -mt-8 text-center m-auto lg:ml-36">
            <Title
              title={data.title}
              style="w-96 mb-16 lg:mb-0 text-center text-blue-900 font-bold text-4xl inline-block"
            />
          </div>
          <div className=" lg:pt-13 m-auto mb-12 text-md h-32">
            <div className="flex  pb-7 leading-8">
              <svg
                id="Icons_Teacher"
                overflow="hidden"
                width={20}
                version="1.1"
                viewBox="0 0 96 96"
                className="w-7 h-7 ml-4  fill-gray-500"
                xmlns="http://www.w3.org/2000/svg"
              >
                {" "}
                <path d=" M 87.8 19 L 23.8 19 C 21.6 19 19.8 20.8 19.8 23 L 19.8 37.5 C 20.9 37.2 22.2 37 23.4 37 C 24.2 37 25 37.1 25.8 37.2 L 25.8 25 L 85.8 25 L 85.8 63 L 51.9 63 L 46.2 69 L 87.8 69 C 90 69 91.8 67.2 91.8 65 L 91.8 23 C 91.8 20.8 90 19 87.8 19" />{" "}
                <path d=" M 23.5 58 C 28.2 58 32 54.2 32 49.5 C 32 44.8 28.2 41 23.5 41 C 18.8 41 15 44.8 15 49.5 C 14.9 54.2 18.8 58 23.5 58" />{" "}
                <path d=" M 56.2 48.1 C 54.9 46.1 52.3 45.6 50.3 46.8 C 49.9 47 49.7 47.4 49.5 47.6 L 34.9 62.8 C 33.5 62.1 32 61.5 30.5 61 C 28.2 60.6 25.8 60.1 23.5 60.1 C 21.2 60.1 18.8 60.5 16.5 61.2 C 13.1 62.1 10.1 63.8 7.6 65.9 C 7 66.5 6.5 67.4 6.3 68.2 L 4.2 77 L 34.1 77 L 34.1 76.9 L 42.6 67 L 55.7 53.2 C 56.9 52 57.3 49.7 56.2 48.1" />
              </svg>
              نام: {data?.teacher?.fullName}
            </div>
            <div className="flex pb-7 leading-8">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="w-7 h-7 ml-4 bi bi-diagram-3"
                viewBox="0 0 16 16"
              >
                <path
                  fill-rule="evenodd"
                  d="M6 3.5A1.5 1.5 0 0 1 7.5 2h1A1.5 1.5 0 0 1 10 3.5v1A1.5 1.5 0 0 1 8.5 6v1H14a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0v-1A.5.5 0 0 1 2 7h5.5V6A1.5 1.5 0 0 1 6 4.5v-1zM8.5 5a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1zM0 11.5A1.5 1.5 0 0 1 1.5 10h1A1.5 1.5 0 0 1 4 11.5v1A1.5 1.5 0 0 1 2.5 14h-1A1.5 1.5 0 0 1 0 12.5v-1zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1zm4.5.5A1.5 1.5 0 0 1 7.5 10h1a1.5 1.5 0 0 1 1.5 1.5v1A1.5 1.5 0 0 1 8.5 14h-1A1.5 1.5 0 0 1 6 12.5v-1zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1zm4.5.5a1.5 1.5 0 0 1 1.5-1.5h1a1.5 1.5 0 0 1 1.5 1.5v1a1.5 1.5 0 0 1-1.5 1.5h-1a1.5 1.5 0 0 1-1.5-1.5v-1zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1z"
                />
              </svg>
              ظرفیت : {data.capacity}
            </div>
            <div className="flex pb-7 leading-8">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                className="w-7 h-7 ml-5"
              >
                {" "}
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M12 6v12m-3-2.818l.879.659c1.171.879 3.07.879 4.242 0 1.172-.879 1.172-2.303 0-3.182C13.536 12.219 12.768 12 12 12c-.725 0-1.45-.22-2.003-.659-1.106-.879-1.106-2.303 0-3.182s2.9-.879 4.006 0l.415.33M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
              شهریه :{data.cost} تومان
            </div>
          </div>
          <div className="pt-13 m-auto md:hidden xl:block mb-12 h-32">
            <div className="flex pb-7 leading-8">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                className="w-7 h-7 ml-5"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M4.26 10.147a60.436 60.436 0 00-.491 6.347A48.627 48.627 0 0112 20.904a48.627 48.627 0 018.232-4.41 60.46 60.46 0 00-.491-6.347m-15.482 0a50.57 50.57 0 00-2.658-.813A59.905 59.905 0 0112 3.493a59.902 59.902 0 0110.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.697 50.697 0 0112 13.489a50.702 50.702 0 017.74-3.342M6.75 15a.75.75 0 100-1.5.75.75 0 000 1.5zm0 0v-3.675A55.378 55.378 0 0112 8.443m-7.007 11.55A5.981 5.981 0 006.75 15.75v-1.5"
                />
              </svg>
              زمان شروع :{data.startDate}
            </div>
            <div className="flex pb-7 leading-8">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                className="w-7 h-7 ml-5"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M5.25 5.653c0-.856.917-1.398 1.667-.986l11.54 6.348a1.125 1.125 0 010 1.971l-11.54 6.347a1.125 1.125 0 01-1.667-.985V5.653z"
                />
              </svg>
              زمان پایان : {data.endDate}
            </div>
          </div>
        </div>
        <div>
          <Image
            className="m-auto hidden lg:block "
            src={data.lesson.image}
            alt="CourseInfo"
            width={450}
            height={0}
          />
        </div>
        {/* {isIncludeCourse && logUser && (
          <button className="bg-blue-400 w-32 h-12 text-white rounded-lg mr-auto">
            حذف از دوره
          </button>
        )} */}
       
        {/* {  isIncludeCourse ? (
            <button
              onClick={handleShopping}
              className="bg-blue-400 w-32 h-12 text-white rounded-lg mr-auto"
            >
              خرید دوره
            </button>
          ) : (
            <button className="bg-blue-400 w-32 h-12 text-white rounded-lg mr-auto">
              حذف از دوره
            </button>
          )} */}
      </div>
      {isIncludeCourse ? (
          <button
            onClick={handleDeleteHistory}
            className="bg-red-400 block mt-10 w-40 h-12 text-white rounded-lg m-auto"
          >
            حذف دوره از سبد خرید
          </button>
        ) : (
          <button
            onClick={handleShopping}
            className="bg-blue-400 block mt-10 w-52 h-12 text-white rounded-lg m-auto"
          >
            اضافه کردن دوره به سبد خرید
          </button>
        )}
    </div>
  );
};

export default Intro;
