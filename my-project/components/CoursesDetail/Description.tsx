import React, { useEffect } from 'react'
import NewsTitle from '../common/NewsTitle'
import AOS from 'aos';
import 'aos/dist/aos.css';

interface Props {
  data:{
  title: string
  cost: number
  endDate: string
  startDate: string
  capacity: number
  image: string
  description: string
  teacher: {
    fullName: string
    email: string
    _id: string
    profile: string
  },
  lesson: {
    _id: string
    lessonName: string
    topics: string[],
    image: string
  },
}}
const Description = ({ data }: Props) => {
  useEffect(() => {
    AOS.init()
}, [])
  return (
    <div className='  max-w-7xl m-auto ' data-aos="zoom-in" data-aos-duration="2000">
      <div className='mb-10 '><NewsTitle Title='توضیحات دوره '></NewsTitle></div>
      <div className=' md:px-10 text-gray-500 leading-9 mt-16'>
        <h2 className='text-black mb-7  text-xl '>{data.title}</h2>
        <p> {data.description}</p>
      </div>



    </div>
  )
}

export default Description