import React from "react";
import Title from "../common/Title";
import Header from "../common/Header";
import Button from "../common/Button";
import Link from "next/link";
import Image from "next/image";
// import Button from "next/Button";

import { useContext } from "react";
import { HistoryContext } from "@/core/context/HistoryContext";

const ShopingCardInfo = () => {
  const { history, handleDelete } = useContext(HistoryContext);
  console.log(history.length);
  const handleCost = () => {
    // let initalCost = 0;
    const finalCost = history.reduce(
      (acc: any, item: any) => acc + item.cost,
      0
    );
    return finalCost;
  };
  return (
    <div>
      <Header />
      <h1 className="text-blue-950 text-4xl border-r-4 border-yellow-300 inline-block px-4 w-62 mr-16">
        سبد خرید
      </h1>

      <div className="grid grid-cols-1 md:grid-cols-3">
        <div className="col-span-2 lg:mr-12 mr-0">
          <section
            className="container  px-4 mx-auto"
            style={{ width: "100%" }}
          >
            <div className="flex flex-col mt-6">
              <div className=" overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                  <div className="overflow-hidden border border-gray-200 dark:border-gray-700 md:rounded-lg">
                    <table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
                      <thead className="bg-gray-50 dark:bg-gray-800">
                        <tr>
                          <th
                            scope="col"
                            className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            <button className="flex items-center gap-x-3 focus:outline-none">
                              <span>ردیف</span>
                            </button>
                          </th>
                          <th
                            scope="col"
                            className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            <button className="flex items-center gap-x-3 focus:outline-none">
                              <span>نام دوره</span>
                            </button>
                          </th>
                          <th
                            scope="col"
                            className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            <button className="flex items-center gap-x-3 focus:outline-none">
                              <span> تاریخ شروع</span>
                            </button>
                          </th>

                          <th
                            scope="col"
                            className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            <button className="flex items-center gap-x-3 focus:outline-none">
                              <span> قیمت دوره</span>
                            </button>
                          </th>
                          <th
                            scope="col"
                            className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            <button className="flex items-center gap-x-3 focus:outline-none">
                              <span> حذف دوره</span>
                            </button>
                          </th>
                        </tr>
                      </thead>
                     
                      {history.map((item: any, index: number) => (
                        <tbody
                          key={index}
                          className="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900"
                        >
                          <tr>
                            <td className="px-4 py-4 text-sm font-medium whitespace-nowrap">
                              <div>
                                <p className="text-sm font-normal text-gray-600 dark:text-gray-400">
                                  {++index}
                                </p>
                              </div>
                            </td>
                            <td className="px-4 py-4 text-sm whitespace-nowrap">
                              <div>
                                <h4 className="text-gray-700 dark:text-gray-200">
                                  {" "}
                                  {item.title}
                                </h4>
                              </div>
                            </td>
                            <td className="px-4 py-4 text-sm whitespace-nowrap">
                              <div>
                                <h4 className="text-gray-700 dark:text-gray-200">
                                  {item.startDate}
                                </h4>
                              </div>
                            </td>

                            <td className="px-4 py-4 text-sm whitespace-nowrap">
                              <div>
                                <h4 className="text-gray-700 dark:text-gray-200">
                                  {item.cost}
                                </h4>
                              </div>
                            </td>
                            <td className=" text-sm whitespace-nowrap">
                              <div
                                onClick={() => handleDelete(item.courseId)}
                                className="cursor-pointer w-6 h-6 mr-8"
                              >
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke-width="1.5"
                                  stroke="currentColor"
                                  class="w-6 h-6 text-red-500"
                                >
                                  <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                  />
                                </svg>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      ))}
                      {/* ))} */}
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div className="col-span-1">
          <div className=" grid-rows-3 border border-gray-300 rounded-lg shadow-xl mt-6 mr-6 ml-7">
            <div className="row-span-1 py-3 pr-3 border rounded-lg border-b-gray-300 ">
              <p>مدیریت صورت حساب</p>
            </div>
            <div className="row-span-1 py-3 pr-3 border rounded-lg border-b-gray-300  ">
              <p>
                تعداد کل :
                {history.length ? (
                  <span>{history.length}</span>
                ) : (
                  <span>دوره ای موجود نیست</span>
                )}
              </p>
            </div>
            <div className="row-span-1 py-3 pr-3 border rounded-lg ">
              جمع کل :{handleCost()}
            </div>
          </div>
          <div className="w-[90%] m-auto ">
            <Link href="./Courses">
              <Button
                title="پرداخت"
                class=" h-12 mt-3 w-full active:bg-blue-700 text-white hover:bg-blue-500 bg-sky-400 rounded-md shadow-2xl"
              />
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShopingCardInfo;
