import React from 'react'
import TableCourse from './TableCourse'
import Title from '../common/Title'
import AOS from 'aos';
import 'aos/dist/aos.css';
import {useEffect} from 'react';

interface data {
  _id: string;
  title: string;
  cost: number;
  endDate: string;
  startDate: string;
  capacity: number;
  image: string;
  description: string;
  teacher: {
    fullName: string;
    email: string;
    _id: string;
    profile: string;
  };
  lesson: {
    _id: string;
    lessonName: string;
    topics: string[];
    image: string;
  };
}
// get course by student id api call anjam she .. miran to state bade map mikhore
const MyCourses = ( {course} : {course:data}) => {
  useEffect(() => {
    AOS.init()
}, [])
  return (
      <div  data-aos="zoom-in-right" data-aos-duration="1500" className='bg-white h-auto max-w-7xl grid mt-16 mb-10 shadow-2xl rounded-lg m-auto ' style={{width:"100%"}} >
        <div className=' pr-7'>
        <div className="flex mt-9 mb-4 justify-center lg:justify-start  ">
          <Title title="دوره های من"  style=" max-w-7xl text-center text-blue-900 text-3xl  border-r-4 border-yellow-300 inline-block m-auto md:px-4 " />
          </div>
        </div>
        <TableCourse course={course} />
      </div>
  )
}

export default MyCourses