export default function Layout({ children }) {
  return (
    <div className=" max-w-7xl m-auto flex lg:mr-52 rounded-3xl bg-white  h-full  pb-8" style={{width:"82%"}} >
     <div className="m-auto" style={{width:"100%"}}  >
      {children}
     </div> 
     </div>
  )
}