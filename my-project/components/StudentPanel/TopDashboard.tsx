/* import React from 'react'
import TopDashboardCardInfo from './TopDashboardCardInfo'

const TopDashboard = () => {
  return (
    <div className='max-w-7xl '>
      <TopDashboardCardInfo />
    </div>
  )
}

export default TopDashboard */
import React from 'react'
import Image from 'next/image'
import Link from 'next/link';

interface Props {
  Title: string;
  h2: string;
  svg: string;
  display: string;
  href:string
}

const TopDashboard = (props: Props) => {
  return (
    <>
    <Link href={props.href}>
    <div className={`shadow-md ml-5 p-4 rounded-lg w-40 h-28 mt-6 m-2 mr-4 max-w-7xl ${props.display === "true" ? "hidden" : ""} lg:${props.display === "true" ? "block" : ""}`}>
      <div className='inline text-blue-800 '>
        <Image
          className='inline ml-3 bg-blue-200 rounded-xl'
          src={props.svg}
          alt="create"
          width={20}
          height={20}
        />
        {props.Title}
      </div>
      <div className='text-gray-500 text-sm mt-2'>
        {props.h2}
      </div>
    </div>
    </Link>
    </>
  );
};

export default TopDashboard;