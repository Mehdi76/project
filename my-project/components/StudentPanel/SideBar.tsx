import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import Cookie from "js-cookie";
import React, { useState, useEffect } from "react";
const data = [
  {
    title: "داشبورد",
    href: "/StudentPanel/Dashboard",
    svg: "/assets/images/StudentPanel/Dashboard.svg",
  },
  {
    title: " ویرایش اطلاعات",
    href: "/StudentPanel/EditInfo",
    svg: "/assets/images/StudentPanel/EditInfo.svg",
  },
  {
    title: "   دوره های من",
    href: "/StudentPanel/MyCourses",
    svg: "/assets/images/StudentPanel/MyCourses.svg",
  },
  {
    title: "   خروج",
    href: "/Landing",
    svg: "/assets/images/StudentPanel/Logout.svg",
  },
];

const SideBar = () => {
  const [state, setState] = useState(false);
  const [user, setUser] = useState([]);
  const router = useRouter();
  router.pathname === "../Dashboard" && "bg-gray-200";

  useEffect(() => {
    const res = Cookie.get("user");
    const user = JSON.parse(res);
    setUser(user);
  }, []);

  useEffect(() => {
    console.log(user);
  }, [user]);

  return (
    <div
      className={`max-w-7xl m-auto absolute top-7 z-99 ${
        !state ? "right-7" : " right-52"
      } `}
      style={{ width: "100%" }}
    >
      <button
        onClick={() => setState(!state)}
        data-drawer-target="logo-sidebar"
        data-drawer-toggle="logo-sidebar"
        aria-controls="logo-sidebar"
        type="button"
        className=" flex-inline mr-3 items-center p-2 mt-2 ml-3 text-sm text-gray-500 rounded-lg lg:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
      >
        <span className="sr-only">Open sidebar</span>
        <svg
          className="w-6 h-6"
          aria-hidden="true"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            clip-rule="evenodd"
            fill-rule="evenodd"
            d="M2 4.75A.75.75 0 012.75 4h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 4.75zm0 10.5a.75.75 0 01.75-.75h7.5a.75.75 0 010 1.5h-7.5a.75.75 0 01-.75-.75zM2 10a.75.75 0 01.75-.75h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 10z"
          ></path>
        </svg>
      </button>

      <aside
        id="logo-sidebar"
        className={`shadow-xl  fixed top-0 right-0 z-40 xl:w-52 lg:w-52 h-screen transition-transform -translate-x-0 sm:translate-x-0  ${
          !state ? "hidden" : ""
        }  lg:block`}
        aria-label="Sidebar"
      >
        <div className="h-full px-3 py-0 rounded-sm bg-gray-200 dark:bg-gray-800">
          <div className=" -mx-6 px-6 py-4 ">
            <Link
              href="../Landing"
              title="home"
              className="flex items-center pl-2.5 mb-2"
            >
              <Image
                src="/assets/images/StudentPanel/logo.png"
                alt="bahrlogo"
                width={40}
                height={0}
              />
              <h2 className="text-blue-700 text-md pr-3 leading-10self-center font-semibold whitespace-nowrap dark:text-white">
                آکادمی کدنویسی بحر
              </h2>
            </Link>
            <hr className="w-8/12 m-auto text-gray-200" />
          </div>
          <div className="mt-1 text-center">
            <Image
              src="/assets/images/StudentPanel/emilia.png"
              className="w-10 h-10 m-auto rounded-full object-cover lg:w-24 lg:h-24"
              alt="avatar"
              width={100}
              height={100}
            />
            <h5 className="hidden mt-4 mb-1 text-lg font-semibold text-gray-600 lg:block">
              {user.fullName}
            </h5>
            <span className="hidden text-sm text-gray-400 lg:block">
              دانش آموز
            </span>
          </div>
          <div className="bg-gray-200">
            <div className="w-fit p-0">
              {data.map((item, index) => (
                <div key={index} className="">
                  <Link href={item.href}>
                    <div
                      className={`flex mt-3 mr-1 h-10 leading-10 pr-2 rounded-2xl w-44 hover:bg-gray-100 active:bg-white  focus:outline-none focus:ring focus:ring-violet-300  ${
                        router.pathname === item.href && "bg-white"
                      }`}
                    >
                      <Image
                        src={item.svg}
                        className="ml-4 "
                        alt="Dashboard"
                        width={25}
                        height={10}
                      />
                      {item.title}
                    </div>
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </div>
      </aside>
    </div>
  );
};
export default SideBar;
