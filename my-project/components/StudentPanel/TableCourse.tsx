
import React, { useContext, useEffect, useState } from 'react'
import PaginationNav1 from '../common/Pagination'
import Link from 'next/link'
import Image from 'next/image'
import { HistoryContext } from '@/core/context/HistoryContext'

/* const data = [
  {
    id: "1",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
  {
    id: "2",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
  {
    id: "3",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
  {
    id: "4",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
  {
    id: "5",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
  {
    id: "6",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
  {
    id: "7",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
  {
    id: "8",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
  {
    id: "9",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
  {
    id: "10",
    title: " دوره پیشرفته React",
    teacher: " استاد بحر العلوم",
    start: " 01/02/25",
    capacity: "35",
    price: "125.0000 T",
    delete: "/assets/images/StudentPanel/delete.svg"
  },
] */
interface data {
  _id: string;
  title: string;
  cost: number;
  endDate: string;
  startDate: string;
  capacity: number;
  image: string;
  description: string;
  teacher: {
    fullName: string;
    email: string;
    _id: string;
    profile: string;
  };
  lesson: {
    _id: string;
    lessonName: string;
    topics: string[];
    image: string;
  };
}

const TableCourse = ({course}:{course:data}) => {
   const {history}  = useContext(HistoryContext)
    const [pageIndex, setPageIndex] = useState(0);
  const pageCount = history.length / 3;
 /*  useEffect(() => {
    console.log(history)
  }, [history]) */
  
  return (

      <section className="container  px-4 mx-auto w-[98%]">
        <div className="flex flex-col mt-6">
          <div className=" w-[58%] sm:w-[80%] md:[90%] lg:w-[95%] xl:w-[100%] overflow-x-auto">
            <div className=" w-full inline-block min-w-full my-2 align-middle">
              <div className=" w-full border border-gray-200 dark:border-gray-700 md:rounded-lg">
                <table className=" w-full divide-y divide-gray-200 dark:divide-gray-700">
                  <thead className="bg-gray-50 dark:bg-gray-800">
                    <tr>
                      <th scope="col" className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <button className="flex items-center gap-x-3 focus:outline-none"><span>ردیف</span></button>
                      </th>
                      <th scope="col" className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <button className="flex items-center gap-x-3 focus:outline-none"><span>نام دوره</span></button>
                      </th>
                      <th scope="col" className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <button className="flex items-center gap-x-3 focus:outline-none"><span>  مدرس دوره</span></button>
                      </th>
                      <th scope="col" className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <button className="flex items-center gap-x-3 focus:outline-none"><span>  تاریخ شروع</span></button>
                      </th>
                      <th scope="col" className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <button className="flex items-center gap-x-3 focus:outline-none"><span>ظرفیت</span></button>
                      </th>
                      <th scope="col" className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <button className="flex items-center gap-x-3 focus:outline-none"><span>  قیمت دوره</span></button>
                      </th>
                    </tr>
                  </thead>
                  {history.slice(((pageIndex + 1) * 4) - 4, ((pageIndex + 1) * 4)) .map((item:data, index:string) => (
                  <tbody key={index} className="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900">
                    <tr >
                      <td className="px-4 py-4 text-sm font-medium whitespace-nowrap">
                        <div>
                          <p className="text-sm font-normal text-gray-600 dark:text-gray-400"> {course._id}</p>
                        </div>
                      </td>
                      <td className="px-4 py-4 text-sm whitespace-nowrap">
                        <div>
                          <h4 className="text-gray-700 dark:text-gray-200"> {course.title}</h4>
                        </div>
                      </td>
                      <td className="px-4 py-4 text-sm whitespace-nowrap">
                        <div>
                          <h4 className="text-gray-700 dark:text-gray-200">{course.teacher.fullName}</h4>
                        </div>
                      </td>
                      <td className="px-4 py-4 text-sm whitespace-nowrap">
                        <div>
                          <h4 className="text-gray-700 dark:text-gray-200">{course.startDate}</h4>
                        </div>
                      </td>
                      <td className="px-4 py-4 text-sm whitespace-nowrap">
                        <div>
                          <h4 className="text-gray-700 dark:text-gray-200">{course.capacity}</h4>
                        </div>
                      </td>
                      <td className="px-4 py-4 text-sm whitespace-nowrap">
                        <div>
                          <h4 className="text-gray-700 dark:text-gray-200">{course.cost}</h4>
                        </div>
                      </td>
                      
                    </tr>
                  </tbody>
                  ))}
                </table>
              </div>
            </div>
          </div>
        </div>

      
     


      <div className="py-2">
        <PaginationNav1
          gotoPage={setPageIndex}
          canPreviousPage={pageIndex > 0}
          canNextPage={pageIndex < pageCount - 1}
          pageCount={pageCount}
          pageIndex={pageIndex}
        />
      </div>

</section>
  )
}

export default TableCourse