import React from "react";
import DatePicker from "react-multi-date-picker";
import persian from "react-date-object/calendars/persian";
import persian_fa from "react-date-object/locales/persian_fa";
const DatePickerStudent = () => {
  return (
    <div>
       <DatePicker calendar={persian} locale={persian_fa}   inputClass="custom-input" 
         style={{
          width:"288px",
          height: "40px",
          borderRadius: "8px",
          fontSize: "14px",
          padding: "3px 10px",
        }}
       />
    </div>
  )
}

export default DatePickerStudent