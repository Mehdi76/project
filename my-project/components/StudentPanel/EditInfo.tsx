import React from "react";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as yup from "yup";
import InputForm from "../common/InputForm";
import Button from "../common/Button";
import Image from "next/image";
import Title from "../common/Title";
import DatePicker from "react-multi-date-picker";
import AOS from 'aos';
import 'aos/dist/aos.css';
import {useEffect} from 'react';

// inital value mishe chizi k az state user miad va state user ham mesle side bar
const EditInfo = () => {
  const validation = yup.object().shape({
    name: yup
      .string()
      .required(" لطفا نام خود را وارد کنید ")
      .matches(/^[آ-ی]*$/, " فرمت نام اشتباه می باشد"),

    mobile: yup
      .string()
      .required(" لطفا شماره موبایل خود را وارد کنید ")
      .matches(/^09[0|1|2|3|9][0-9]*$/, " فرمت وارد شده درست نیست ")
      .min(11, "تعداد شماره های وارد شده درست نیست")
      .max(11, "تعداد شماره های وارد شده درست نیست"),

    email: yup
      .string()
      .email("فرمت ایمیل اشتباه می باشد")
      .required(" لطفا ایمیل خود را وارد کنید "),
  });

  const handleSubmit = (value: any) => {
    console.log(value);
  };

  useEffect(() => {
    AOS.init()
}, [])
  return (

    <div data-aos="zoom-out-up" data-aos-duration="2000" className="grid max-w-7xl mt-7 shadow-2xl rounded-2xl bg-white m-auto " style={{width:"100%"}}>
      <section className=" w-auto h-auto ">
        <div className="flex mt-7 mb-5 justify-center lg:justify-start lg:mr-12  ">
          <Title title="ویرایش اطلاعات کاربری" style=" max-w-7xl text-center text-blue-900 text-3xl  border-r-4 border-yellow-300 inline-block px-4 " />
        </div>
        <div className="">
          <figure>
            <Image
              className="m-auto"
              src="/assets/images/StudentPanel/user.PNG"
              alt="user"
              width={200}
              height={100}
            />
          </figure>
          <Formik
            initialValues={{
              email: "",
              name: "",
              mobile: "",
            }}
            onSubmit={handleSubmit}
            validationSchema={validation}
          >
            <div className=" ">
              <Form className="grid grid-cols-1 lg:grid-cols-2  gap-5 mt-4 ">
                <div className=" w-64 m-auto md:pr-7">
                  <div className=" h-24  ">
                    <InputForm title=" نام کاربری" name="name" />
                  </div>

                  <div className=" h-24 ">
                    <InputForm title=" ایمیل کاربر" name="email" />
                  </div>
                </div>

                <div className="w-64 md:pr-7  xl:mr-5 m-auto">
                  <div className="h-20 w-96 mb-3 ">
                    <p className="text-gray-500 text-sm w-72">تاریخ تولد</p>
                    <DatePicker style={{  width:"320px",height: "40px",borderRadius: "8px", fontSize: "14px",padding: "3px 10px",}}/>
                  </div>

                  <div className=" h-24">
                    <InputForm title=" شماره تماس" name="mobile" />
                  </div>
                </div>
              </Form>
            </div>
          </Formik>
        </div>
        <div className="flex flex-row justify-center mt-6 pb-12">
          <div className="ml-2">
            <Button
              title="ثبت تغییرات"
              class="w-36 p-2 shadow-xl active:bg-green-700 hover:bg-green-400 bg-green-500  shadow-green-500/50 text-teal-50 rounded-md"
            />
          </div>
          <div>
            <Button
              title="لغو تغییرات"
              class="w-36 p-2 bg-emerald-50-400 active:bg-red-700 active:text-white hover:bg-red-200 text-red-700 border border-red-700 rounded-md shadow-2xl"
            />
          </div>
        </div>
      </section>
    </div>
  );
};

export default EditInfo;
