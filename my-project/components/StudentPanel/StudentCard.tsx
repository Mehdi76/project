import React, { useEffect } from "react";
import {useState} from 'react';
import Image from "next/image";
import PaginationNav1 from "../common/Pagination";
import Button from "../common/Button";
import { useRouter } from "next/router";
import AOS from 'aos';
import 'aos/dist/aos.css';

const courseData = [
  {
  
    src:"/assets/images/Courses/html5.png" ,
    courseName: "HTML 5",
    teacher: " دکتر  ایرج ",
    price: " 500.000 تومان",
    studentCount: 10,
    animation:"fade-up",
    delay:"2000"

  },
  {
    
    src:"/assets/images/Courses/React.png" ,
    courseName: "React",
    teacher: " دکتر صدر",
    price: " 700.000 تومان",
    studentCount: 8,
    animation:"fade-right",
    delay:"2000"
  },
  {
    
    src:"/assets/images/Courses/python.png" ,
    courseName: "Python",
    teacher: " دکتر نیک رو",
    price: " 1.000.000 تومان",
    studentCount: 5,
    animation:"fade-left",
    delay:"2000"
  },
  {
    
    src:"/assets/images/Courses/java.png" ,
    courseName: "Java",
    teacher: " دکتر راد",
    price: " 250.000 تومان",
    studentCount: 10,
    animation:"fade-down",
    delay:"2000"
  },
  
]; 

const StudentCard = () => {

  const router = useRouter();
  useEffect(() => {
    AOS.init()
}, [])
  
  return (
    <>
      <div className="grid xl:grid-cols-4  lg:grid-cols-3 sm:grid-cols-2 mx-auto sm:p-3 max-w-7xl "style={{width:"100%"}}>
      {courseData.map((item, index) => (
         <div key={index} data-aos={item.animation} data-aos-duration={item.delay} className="max-w-sm m-2  rounded-xl overflow-hidden shadow-lg h-72 w-44 grid grid-rows-2">
         <div className="flex relative items-center justify-center bg-blue-100">
           <div className="w-5 h-5 absolute top-1 left-1 text-xs ml-1 text-center ">
           <svg xmlns="http://www.w3.org/2000/svg"fill="none"viewBox="0 0 24 24" stroke-width="1.5"stroke="currentColor"
             className="" 
           ><path stroke-linecap="round"stroke-linejoin="round" d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z"
             />
           </svg>
              {item.studentCount}
           </div>
           <Image
             src={item.src}
             alt="card"
             width={125}
             height={150}
           />
         </div>
         
           <div className="px-3 py-2" >
             <div className=" text-2xl mb-2 text-center text-blue-900">
               {item.courseName}
             </div>
             <div className=" flex justify-between text-xs mt-4  ">
               <div className="flex">
                 <div className="flex">
                   <svg id="Icons_Teacher"overflow="hidden" width={20} version="1.1" viewBox="0 0 96 96" xmlns="http://www.w3.org/2000/svg"> <path d=" M 87.8 19 L 23.8 19 C 21.6 19 19.8 20.8 19.8 23 L 19.8 37.5 C 20.9 37.2 22.2 37 23.4 37 C 24.2 37 25 37.1 25.8 37.2 L 25.8 25 L 85.8 25 L 85.8 63 L 51.9 63 L 46.2 69 L 87.8 69 C 90 69 91.8 67.2 91.8 65 L 91.8 23 C 91.8 20.8 90 19 87.8 19" /> <path d=" M 23.5 58 C 28.2 58 32 54.2 32 49.5 C 32 44.8 28.2 41 23.5 41 C 18.8 41 15 44.8 15 49.5 C 14.9 54.2 18.8 58 23.5 58" /> <path d=" M 56.2 48.1 C 54.9 46.1 52.3 45.6 50.3 46.8 C 49.9 47 49.7 47.4 49.5 47.6 L 34.9 62.8 C 33.5 62.1 32 61.5 30.5 61 C 28.2 60.6 25.8 60.1 23.5 60.1 C 21.2 60.1 18.8 60.5 16.5 61.2 C 13.1 62.1 10.1 63.8 7.6 65.9 C 7 66.5 6.5 67.4 6.3 68.2 L 4.2 77 L 34.1 77 L 34.1 76.9 L 42.6 67 L 55.7 53.2 C 56.9 52 57.3 49.7 56.2 48.1" />
                   </svg>
                   
                 </div>
                 <p>{item.teacher}</p>
               </div>
               <div className="text-red-700">{item.price} </div>
             </div>
             <Button  onClick={() => router.push(`Courses/${index}`)} title="نمایش جزئیات" class="flex justify-center text-white mx-auto my-6 w-36 h-9 p-1 rounded-md bg-blue-600" />
           </div>
          
       </div>
       
       ))}
     </div>

    </>
  )
};

export default StudentCard;
