
import 'react-calendar/dist/Calendar.css';
import StudentCard from './StudentCard';
import CalenderCustom from '../common/CalendarCom';
import AOS from 'aos';
import 'aos/dist/aos.css';
import {useEffect} from 'react';
import TopDashboard from './TopDashboard';
import { getCookie } from '@/core/services/storage/cookies.storage';


const topData = [{
  Title:" سبد خرید",
  href:"../ShopingCard" ,
  h2:" مشاهده دوره های خریداری شده",
  svg:"/assets/images/StudentPanel/plus-circle.svg",
  display:"false"
},
{
  Title:"خرید دوره",
  href:"../../Courses " ,
  h2:"  خرید دوره های آموزشی جامع",
  svg:"/assets/images/StudentPanel/plus-circle.svg",
  display:"false"
},
{
  Title:"ثبت نظر ",
  href:"../../Courses " ,
  h2:" ثبت نظر در بخش اخبار و مقالات",
  svg:"/assets/images/StudentPanel/plus-circle.svg",
  display:"true"
}
]
interface data {
  role : string
  fullName: string
  email: string
  phoneNumber: number
  birthDate: number
  nationalId: number
  profile: string
}
interface allData{
  _id: string
  role : string
  fullName: string
  email: string
  phoneNumber: number
  birthDate: number
  nationalId: number
  profile: string
}
const DashboardCom = ({user}: {user:data}) => {

  useEffect(() => {
    AOS.init()
}, [])
  return (
    <div data-aos="zoom-in-up" data-aos-duration="1500" className=' mt-12 rounded-2xl ' style={{width:"100%"}}>
      <div className=' max-w-7xl '>
        <div className='grid xl:grid-cols-5 m-auto lg:grid-cols-3 w-48 sm:w-fit mb-4 ' >
          <div className='xl:col-span-4 lg:col-span-4  xl:px-px xl:mr-1 bg-white flex rounded-2xl shadow-lg border border-gray-200 sm:m-auto mt-6 '>
            <div className=' w-36 text-center text-blue-800 text-2xl xl:block hidden mt-10 mr-2'> دسترسی 
            <h2 className=' w-36  text-center text-blue-800 text-2xl xl:block hidden mb-8'> سریع
              
              </h2>
            </div>
            {topData.map((item, index) => (
        <TopDashboard
          key={index}
          href={item.href} 
          Title={item.Title}
          h2={item.h2}
          svg={item.svg}
          display={item.display}
        />
      ))}
          </div>
          <div className='xl:col-span-1 lg:col-span-2 xl:pl-5 xl:ml-6 lg:pl-14 hidden xl:flex mr-4  '>
          <CalenderCustom readOnly/>
            </div>
        </div>
        <div className=' bg-white flex m-auto max-w-7xl w-full md:m-auto border border-gray-100 shadow-2xl rounded-2xl  '>
          <h2 className=' w-28  mr-3  text-center text-blue-800 text-2xl xl:block hidden mt-12  leading-loose mb-12 border-yellow-300 border-r-4 h-24 pr-3 '>اطلاعات ثبت شده</h2>
          <div className=' flex '>
          
            <div className='mr-8 grid grid-cols-2 mt-9 '>
              <div className='col-span-1 w-96 mr-8  '>
              <p  className='mb-8'>نقش : {user.role} </p>
              <p className='mb-8'>نام کاربر : {user.fullName}</p>
              <p className='mb-8'>ایمیل : {user.role}</p>
              </div>
              <div className='col-span-1 w-96'>
              <p className='mb-8'>شماره موبایل : {user.phoneNumber}</p>
              <p className='mb-8'>شماره ملی : {user.email}</p>
              <p className='mb-8'> تولد : {user.birthDate}</p>
              </div>
            </div>
      </div>
        </div>

      </div>
    </div>
  )
}

export default DashboardCom