import React from "react";

interface Props {
  show1: string;
  showHandler: boolean;
  setHandler(showHandler: boolean): any;
}

const Hamber = (props: Props) => {
  return (
    <div
      className={`flex items-center justify-center lg:hidden`}
    >
      <div
        onClick={() => props.setHandler(!props.showHandler)}
        className="group flex h-11 w-11 cursor-pointer items-center justify-center rounded-3xl p-2 bg-sky-400 absolute top-7"
      >
        <div className="space-y-2">
          <span
            className={`block h-1 w-8 origin-center rounded-full bg-white transition-transform ease-in-out" ${
              !props.showHandler && "translate-y-1.5 rotate-45"
            }`}
          ></span>
          <span
            className={`block h-1 w-6 origin-center rounded-full bg-white transition-transform ease-in-out  ${
              !props.showHandler && "w-8 -translate-y-1.5 -rotate-45"
            }`}
          ></span>
        </div>
      </div>
    </div>
  );
};

export default Hamber;
