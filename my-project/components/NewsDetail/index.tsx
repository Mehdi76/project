import React from 'react'
import Header from '@/components/common/Header'
import NewsCard from "@/components/News/NewsCard";
import NewsTitle from '@/components/common/NewsTitle';
import Reviews from '@/components/common/Reviews';
import TopCardTitle from '@/components/NewsDetail/TopCardTitle';
import Description from '@/components/NewsDetail/Description';
import Intro from '@/components/NewsDetail/Intro';
import Footer from '@/components/common/Footer';

interface Props {
data:{
    title: string;
    text: string;
    category: string;
    image: string;
}
allData:
{
    _id: string;
    title: string;
    text: string;
    category: string;
    image: string;
}[]
}
const NewsDetailFinal = ({data , allData}:Props) => {
    return (
        <div className='max-w-7xl m-auto '>
            <Header />
            <div className='sm:mx-10 m-auto mx-6'>
            <Intro data={data} />
            <Description data={data} />
            <NewsTitle Title=' مقاله های مرتبط'></NewsTitle>
            <TopCardTitle Title="مقاله های دیگر نویسنده"></TopCardTitle>
              <NewsCard data={allData}/>  
            <TopCardTitle Title="مقاله های مشابه"></TopCardTitle>
            <NewsCard data={allData}/> 
            <Reviews />
            <Footer />
           </div>
        </div>

    )
}

export default NewsDetailFinal