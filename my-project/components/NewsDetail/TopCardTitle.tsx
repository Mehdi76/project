import React from 'react'

interface props{
    Title:string
}

const TopCardTitle = (props:props) => {
  return (
    <div>
        <h2 className='max-w-7xl text-xl text-center text-blue-800 mt-20 mb-10'>{props.Title}</h2>
    </div>
  )
}

export default TopCardTitle