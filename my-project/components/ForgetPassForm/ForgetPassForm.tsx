import React from "react";
import Image from "next/image";
import {
  ErrorMessage,
  Field,
  Form,
  Formik,
  FormikProvider,
  useFormik,
} from "formik";
import * as yup from "yup";
import InputForm from "../common/InputForm";
import Button from "../common/Button";
import Link from "next/link";
import { forgetPassUser } from "@/core/services/api/Auth/forgetPass-api";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useRouter } from "next/router";



const ForgetPassForm = () => {
  const route = useRouter();
  const validation = yup.object().shape({
    email: yup
      .string()
      .email("فرمت ایمیل اشتباه می باشد")
      .required(" لطفا ایمیل خود را وارد کنید "),
  });

  const handleSubmit = async (value: { email: string }) => {
    const val = { email: value.email };
    forgetPassUser(val);
    const forgetPass = await forgetPassUser(val);
    if (forgetPass) {
      toast.success("رمز جدید به ایمیلتان ارسال شد");
      route.push("/");
    } else {
      toast.error("عملیات انجام نشد");
    }

  };

  const formikForgetPass = useFormik({
    initialValues: { email: "" },
    onSubmit: handleSubmit,
    validationSchema: validation,
  });
  return (
    <>
      <div className="w-[380px] h-64 border-2 border-sky-700 rounded-xl mt-10">
        <div>
          <div className="-mt-4 mr-6 text-center bg-white w-40  ">
            <h3 className="text-sky-800 text-2xl"> بازیابی رمز عبور</h3>
            <div>
              <FormikProvider value={formikForgetPass}>
                <Form className="text-center w-full mt-4">
                  <div className="h-20">
                    <InputForm title="ایمیل کاربر" name="email" />
                  </div>
                  <br />
                </Form>
              </FormikProvider>
            </div>

            <br />

            <div className="flex mx-16 w-full -mt-4">
              <div>
                <Button
                  title="تایید ایمیل"
                  class="w-36 p-2 active:bg-blue-700 hover:bg-blue-800   bg-sky-900 text-teal-50 rounded-md shadow-2xl"
                />
                 <ToastContainer />
              </div>
            </div>
            <div className="flex mr-20 w-44 mt-7">
              <Link href={"/Register"}>
                <span className="text-sky-600 relative left-2  ">ثبت نام</span>
              </Link>
              <span className="border-sky-600 border-l-2"></span>
              <Link href={"/Login"}>
                <span className="text-sky-600 relative right-2">ورود</span>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ForgetPassForm;
