import Image from "next/image";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as yup from "yup";
import InputForm from "../common/InputForm";
import Button from "../common/Button";
import RegisterForm from "../RegisterForm/RegisterForm";
import Header from "../common/Header";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

const RegisterPage = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  const handleSubmit = (value: any) => {
    console.log(value);
  };
  return (
    <>
      <section  className="m-auto mt-12">
        <div className="relative mt-[-270px]">
          <Header />
        </div>
        <div data-aos="zoom-in-up" data-aos-duration="2000" className="grid sm:grid-cols-1 md:grid-cols-2 place-items-center relative top-[190px]">
          <RegisterForm />

          <div className="sm:hidden md:block hidden">
            <Image
              src="/assets/images/Login/register.png"
              alt="login"
              height={475}
              width={445}
            />
          </div>
        </div>
      </section>
    </>
  );
};

export default RegisterPage;
