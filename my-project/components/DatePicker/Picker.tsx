import React, { useState } from "react";
import DatePicker, { DateObject } from "react-multi-date-picker";
import persian from "react-date-object/calendars/persian";
import persian_fa from "react-date-object/locales/persian_fa";

interface props {
  className?: string;
  setval: React.Dispatch<React.SetStateAction<string>>;
  val: string;
}

const Picker = (props: props) => {
  return (
    <>
      <DatePicker
        value={props.val}
        calendar={persian}
        locale={persian_fa}
        inputClass="custom-input"
        className={props.className}
        onChange={(e) =>
          props.setval(new DateObject(e).convert(persian, persian_fa).format())
      
        }
      />
    </>
  );
};

export default Picker;
