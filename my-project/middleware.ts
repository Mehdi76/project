import { NextResponse } from "next/server";
// import jwtDecode from "jwt-decode";

export default function middleware(req) {
  const { cookies, url } = req;

  let token = cookies.get("token")?.value;

  if (token) token = JSON.parse(token);
  // let decoded = null;
  // if (token) {
  //   decoded = jwtDecode(token);
  // }

  if (url.includes("/auth/") && token) {
    const redirect_url = new URL("/panel", url);
    return NextResponse.redirect(redirect_url);
  }
  if (url.includes("/panel") && !token) {
    const redirect_url = new URL("/auth/login", url);
    return NextResponse.redirect(redirect_url);
  }

  return NextResponse.next();
}
