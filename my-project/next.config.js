/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['upload.wikimedia.org', 'res.cloudinary.com'],
    imageExtensions: ['.jpg', '.jpeg', '.png', '.gif', '.webp'],
  },
};

module.exports = nextConfig;

